$(function() {

	/* Form Steps
				----------------------------------------------- */
	$("#smart-form").steps({
		headerTag: "h2",
		bodyTag: "fieldset",
		transitionEffect: "fade",
		transitionEffectSpeed: 100,
		enableAllSteps: true,
		enableFinishButton: false,
//		autoFocus: false,
		forceMoveForward: false,
		enablePagination: true,
		labels: {
			finish: "Complete",
			next: "Continue",
			previous: "Go Back",
			loading: "Loading...",
			current: "Current"
		},

		//		Fires when the wizard is initialized.
		onInit: function(event, currentIndex) {
		
		},

		//Fires before the step changes and can be used to prevent step changing by returning false. Very useful for form validation.
		onStepChanging: function(event, currentIndex, newIndex) {

			$('#loader').show();
			
			if (currentIndex > newIndex) {
				return true;
			}
			var form = $(this);
			if (currentIndex < newIndex) {

			}
			if (form.valid()) {
				return true;
			} else {
				$('#loader').hide();
			}
			// return form.valid();
			
		},

		// Fires after the step has change.
		onStepChanged: function(event, currentIndex, priorIndex) {
			$('#loader').hide();
		},

		//		Fires before finishing and can be used to prevent completion by returning false. Very useful for form validation.
		onFinishing: function(event, currentIndex) {
			var form = $(this);

			// Disable validation on fields that are disabled.
			// At this point it's recommended to do an overall check (mean ignoring only disabled fields)
			form.validate().settings.ignore = ":disabled";

			// Start validation; Prevent form submission if false
			return form.valid();
		},

		//		Fires after completion.
		onFinished: function(event, currentIndex) {
			var form = $(this);

			// Submit form input
			form.submit();
		}


	});

});