Bq.CampaignRules = function() {
    var campaignRules = [{
        name: "ZipCode",
        subTitle1: "Get Instant <%= City %> Mortgage Refinance Rates Today!",
        subTitle2: "Compare from hundreds of trusted lenders near you!",
        title: "Enter Your Zip Code:",
        insuranceCompany: "RefinanaceCalculator.com",
        hidePrepoped: !1,
        updateLayout: ["update:header", "update:sidebar", "update:caption"],
        template: "#bq-field-firstpage",
        required: [{
            type: "notEmpty",
            message: "Please enter zip code."
        }, {
            type: "minLength",
            value: 4,
            message: "Enter at least 4 digits."
        }, {
            type: "zipUS",
            message: "Zip Code is invalid."
        }
        ]
    }, {
        name: "State",
        actions: [{
            name: "MilitaryOrVeteran",
            type: "updateField",
            eventName: "onValidChange",
            setValue: "No",
            triggerRender: !0,
            condition: [{
                invertComparison: !0,
                name: "State",
                param: "value",
                value: ["CA"]
            }
            ]
        }
        ]
    }, {
        name: "InterestRatePercentage",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "range",
        explicit: "",
        styled: !0,
        doNotSend: !1,
        active: !0,
        title: "What is the current interest rate of your mortgage?",
        sets: [{
            label: "2.00%",
            value: "2.00"
        }, {
            label: "2.50%",
            value: "2.50"
        }, {
            label: "3.00%",
            value: "3.00"
        }, {
            label: "3.50%",
            value: "3.50"
        }, {
            label: "4.00%",
            value: "4.00"
        }, {
            label: "4.50%",
            value: "4.50"
        }, {
            label: "5.00%",
            value: "5.00"
        }, {
            label: "5.50%",
            value: "5.50"
        }, {
            label: "6.00%",
            value: "6.00"
        }, {
            label: "6.50%",
            value: "6.50"
        }, {
            label: "7.00%",
            value: "7.00"
        }, {
            label: "7.50%",
            value: "7.50"
        }, {
            label: "8.00%",
            value: "8.00"
        }, {
            label: "8.50%",
            value: "8.50"
        }, {
            label: "9.00%",
            value: "9.00"
        }, {
            label: "9.50%",
            value: "9.50"
        }, {
            label: "10.00%",
            value: "10.00"
        }, {
            label: "10.50%",
            value: "10.00+"
        }, {
            label: "11.00%",
            value: "10.00+"
        }
        ],
        value: "4.50",
        required: "What is the current interest rate of your mortgage?"
    }, {
        name: "CurrentLoanType",
        value: null,
        actions: [{
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 17
        }, {
            name: "MilitaryOrVeteran",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "Yes",
            triggerRender: !0,
            condition: [{
                name: "CurrentLoanType",
                param: "value",
                value: ["VA"]
            }
            ]
        }, {
            name: "MilitaryOrVeteran",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "No",
            triggerRender: !0,
            condition: [{
                invertComparison: !0,
                name: "CurrentLoanType",
                param: "value",
                value: ["VA"]
            }
            ]
        }
        ]
    }, {
        name: "MilitaryOrVeteran",
        title: "Have you or your spouse served in the military?",
        type: "radio",
        value: "No",
        actions: [{
            type: "nextStep",
            eventName: "onValidChange"
        }
        ]
    }, {
        name: "CreditRating",
        value: "Good",
        title: "Estimate your credit score: <span> (Most people have Good credit) </span>",
        sets: [{
            value: "Excellent",
            label: "Excellent (>720)"
        }, {
            value: "Good",
            label: "Good (680-719)"
        }, {
            value: "Some Problems",
            label: "Fair (640-679)"
        }, {
            value: "Major Problems",
            label: "Poor (<639)"
        }
        ],
        actions: [{
            type: "jumpOnStep",
            eventName: "onValidChange",
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Reverse mortgage"],
                condition: [{
                    conditionType: "appSettings",
                    name: "popup",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Reverse mortgage"],
                condition: [{
                    conditionType: "popupParams",
                    name: "credit",
                    param: "show",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Reverse mortgage"],
                condition: [{
                    name: "CreditRating",
                    param: "value",
                    value: ["Excellent", "Good", "Some Problems"]
                }
                ]
            }
            ],
            step: 8
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase"],
                condition: [{
                    conditionType: "appSettings",
                    name: "popup",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Purchase"],
                condition: [{
                    conditionType: "popupParams",
                    name: "credit",
                    param: "show",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Purchase"],
                condition: [{
                    name: "CreditRating",
                    param: "value",
                    value: ["Excellent", "Good", "Some Problems"]
                }
                ]
            }
            ],
            step: 5
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Home equity"],
                condition: [{
                    conditionType: "appSettings",
                    name: "popup",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Home equity"],
                condition: [{
                    conditionType: "popupParams",
                    name: "credit",
                    param: "show",
                    value: [!1]
                }
                ]
            }, {
                name: "LoanType",
                param: "value",
                value: ["Home equity"],
                condition: [{
                    name: "CreditRating",
                    param: "value",
                    value: ["Excellent", "Good", "Some Problems"]
                }
                ]
            }
            ],
            step: 6
        }, {
            type: "popup",
            eventName: "onValidChange",
            condition: [{
                name: "CreditRating",
                param: "value",
                value: ["Major Problems"],
                condition: [{
                    conditionType: "popupParams",
                    name: "credit",
                    param: "show",
                    value: [!0]
                }
                ]
            }
            ],
            dataForView: {
                className: "bq-lexington-popup",
                template: "#bq-lexington-popup",
                typeId: ""
            },
            dataForModel: {
                popupName: "credit",
                title: "Negotiate Like a Pro",
                subTitle1: "Knowing your credit score is an important step in the lending process. It may be the difference between a good rate and a <span>GREAT RATE </span>for your Mortgage Loan.",
                subTitle2: "Click Below to Get Your $1 Credit Report and Receive Your Credit Score <span>FREE</span>!*",
                buttons: {
                    learnMore: {
                        link: {
                            funcName: "redirectLink"
                        },
                        linkText: "Continue to FREE* Score >>"
                    }
                },
                buttonMark: "* with enrollment in gofreecredit.com",
                note: "Note: Using this service will not impact your score in any way.",
                close: "No, I am not worried about my credit score."
            }
        }
        ]
    }, {
        name: "PropertyValue",
        type: "range",
        value: "$200,001 - 250,000",
        explicit: "",
        styled: !0,
        title: "Estimate your property value:",
        actions: [{
            type: "hideNotValidSets",
            eventName: "onValidChange",
            name: "FirstMortgageBalance"
        }, {
            type: "hideNotValidSets",
            eventName: "onInitAction",
            name: "FirstMortgageBalance"
        }, {
            type: "hideNotValidSets",
            eventName: "onValidChange",
            name: "TotalMortgageBalance"
        }, {
            type: "hideNotValidSets",
            eventName: "onInitAction",
            name: "TotalMortgageBalance"
        }, {
            type: "updateField",
            name: "FirstMortgageBalance",
            eventName: "onValueChange",
            setDynamicValue: !0,
            setAttributes: {
                hidden: !1
            },
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "FirstMortgageBalance",
            eventName: "onValueChange",
            setAttributes: {
                hidden: !0
            },
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase", "Reverse mortgage"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "FirstMortgageBalance",
            eventName: "onValueChange",
            setValue: "$150,001 - 175,000",
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                invertComparison: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "TotalMortgageBalance",
            eventName: "onValueChange",
            setDynamicValue: !0,
            setAttributes: {
                hidden: !1
            },
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Reverse mortgage"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "TotalMortgageBalance",
            eventName: "onValueChange",
            setValue: "$150,001 - 175,000",
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                invertComparison: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Reverse mortgage"]
                }
                ]
            }
            ]
        }
        ]
    }, {
        name: "PurchaseTimeFrame",
        hidden: !1,
        title: "Great, when do you plan on purchasing your home?",
        value: "Not Sure",
        type: "radio",
        sets: ["0-3 months", "3-6 months", "6-12 months", "12+ months"],
        actions: [{
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 8
        }
        ]
    }, {
        name: "PhoneNumber",
        mark: !1,
        placeholder: "e.g. 5555555555"
    }, {
        name: "FirstMortgageBalance",
        type: "range",
        value: "$150,001 - 175,000",
        explicit: "",
        styled: !0,
        title: "What is the remaining balance on your mortgage?",
        tooltip: "This amount must be less than or equal to your estimated property value.",
        actions: [{
            type: "updateField",
            name: "CashOutAmount",
            eventName: "onValidChange",
            setDynamicValue: !0,
            triggerRender: !0,
            condition: [{
                name: "FirstMortgageBalance",
                param: "value",
                value: [null],
                invertComparison: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }
            ]
        }, {
            type: "hideNotValidSets",
            name: "CashOutAmount",
            eventName: "onValidChange"
        }, {
            type: "hideNotValidSets",
            name: "CashOutAmount",
            eventName: "onInitAction"
        }
        ]
    }, {
        name: "Address",
        title: "Street address:",
        placeholder: "123 Main St."
    }, {
        name: "BirthDate",
        title: "When were you born?",
        styled: !0,
        actions: [{
            type: "nextStep",
            eventName: "onValidChange"
        }
        ]
    }, {
        name: "DownPayment",
        type: "range",
        active: !0,
        hidden: !1,
        explicit: !1,
        title: "How much can you put down as down payment?",
        sets: [{
            label: "0%",
            value: "0%"
        }, {
            label: "5%",
            value: "5%"
        }, {
            label: "10%",
            value: "10%"
        }, {
            label: "15%",
            value: "15%"
        }, {
            label: "20%",
            value: "20%"
        }, {
            label: "25%",
            value: "25%"
        }, {
            label: "30%",
            value: "30%"
        }, {
            label: "40%",
            value: "40%"
        }, {
            label: "50%",
            value: "50%"
        }, {
            label: "More than 50%",
            value: "More than 50%"
        }
        ],
        value: "20%",
        actions: [{
            name: "MilitaryOrVeteran",
            type: "updateField",
            eventName: "onValidChange",
            setValue: "Yes",
            triggerRender: !0,
            condition: [{
                name: "DownPayment",
                param: "value",
                value: ["0%"]
            }
            ]
        }, {
            name: "MilitaryOrVeteran",
            type: "updateField",
            eventName: "onValidChange",
            setValue: "No",
            triggerRender: !0,
            condition: [{
                invertComparison: !0,
                name: "DownPayment",
                param: "value",
                value: ["0%"]
            }
            ]
        }
        ]
    }, {
        name: "CashOutAmount",
        type: "range",
        explicit: "",
        title: "And would you like to borrow additional cash?<span>(Cash from refinancing your home is a great way to pay off debts)</span>",
        styled: !0,
        value: "$1 - 25,000",
        tooltip: "The amount of cash you would like to borrow. Must be less than or equal to the difference between your property value and what’s left of your mortgage balance."
    }, {
        name: "LoanType",
        type: "radio",
        title: "What type of loan are you looking for?",
        sets: [{
            value: "Refinance",
            label: "Refinance"
        }, {
            value: "Purchase",
            label: "Purchase"
        }, {
            value: "Reverse mortgage",
            label: "Reverse Mortgage"
        }, {
            value: "Home equity",
            label: "Home Equity"
        }
        ],
        value: "Refinance",
        required: "Select type of home loan:",
        actions: [{
            name: "CurrentLoanType",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "Not Sure",
            triggerRender: !0,
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase"]
            }
            ]
        }, {
            name: "PurchaseTimeFrame",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "0-3 months",
            setAttributes: {
                hidden: !1
            },
            triggerRender: !0,
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase"]
            }
            ]
        }, {
            name: "PurchaseTimeFrame",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "Not Sure",
            setAttributes: {
                hidden: !0
            },
            triggerRender: !0,
            condition: [{
                invertComparison: !0,
                name: "LoanType",
                param: "value",
                value: ["Purchase"]
            }
            ]
        }, {
            name: "DownPayment",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "20%",
            triggerRender: !0,
            setAttributes: {
                hidden: !1
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase"]
            }
            ]
        }, {
            name: "DownPayment",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "20%",
            triggerRender: !0,
            setAttributes: {
                hidden: !0
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase"],
                invertComparison: !0
            }
            ]
        }, {
            name: "FirstMortgageBalance",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "$50,000 or less",
            triggerRender: !0,
            setAttributes: {
                hidden: !0
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase", "Reverse mortgage"]
            }
            ]
        }, {
            name: "FirstMortgageBalance",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "$150,001 - 175,000",
            triggerRender: !0,
            setAttributes: {
                hidden: !1
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Home equity"]
            }
            ]
        }, {
            name: "TotalMortgageBalance",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "$50,000 or less",
            triggerRender: !0,
            setAttributes: {
                hidden: !0
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase", "Refinance", "Home equity"]
            }
            ]
        }, {
            name: "TotalMortgageBalance",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "$150,001 - 175,000",
            triggerRender: !0,
            setAttributes: {
                hidden: !1
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Reverse mortgage"]
            }
            ]
        }, {
            name: "CashOutAmount",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "0 (No cash)",
            triggerRender: !0,
            setAttributes: {
                hidden: !0
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Purchase", "Reverse mortgage"]
            }
            ]
        }, {
            name: "CashOutAmount",
            type: "updateField",
            eventName: "onValueChange",
            setDynamicValue: !0,
            triggerRender: !0,
            setAttributes: {
                hidden: !1
            },
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Home equity"]
            }
            ]
        }, {
            name: "PropertyValue",
            type: "updateField",
            eventName: "onValueChange",
            setValue: "$200,001 - 250,000",
            triggerRender: !0,
            condition: [{
                name: "LoanType",
                param: "value",
                value: ["Refinance", "Purchase", "Reverse mortgage", "Home equity"]
            }
            ]
        }, {
            type: "nextStep",
            eventName: "onValidChange"
        }
        ]
    }, {
        name: "AnyBankruptcy",
        type: "radio",
        value: "No",
        title: "Have you had bankruptcy or foreclosure in the last 5 years?",
        aliasing: !0
    }, {
        name: "AnyForeclosure",
        value: "No",
        aliasing: !0
    }, {
        name: "EmailAddress",
        mark: !1,
        placeholder: "email@domain.com"
    }, {
        name: "BankruptcyTime",
        value: "More than 5 years",
        aliasing: !0
    }, {
        name: "ForeclosureTime",
        value: "More than 5 years",
        aliasing: !0
    }, {
        name: "PropertyType",
        sets: [{
            value: "Single Family Home",
            label: "Single Family"
        }, {
            value: "Multi-Family Home",
            label: "Multi-Family"
        }, {
            value: "Condo",
            label: "Condo"
        }, {
            value: "Townhouse",
            label: "Townhouse"
        }
        ],
        value: "Single Family Home",
        title: "What type of property is this loan for?",
        actions: [{
            type: "nextStep",
            eventName: "onValidChange"
        }
        ]
    }, {
        name: "PurchaseYear"
    }, {
        name: "PurchasePrice",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        active: !0,
        title: "How much did you originally purchase your home for?<span>(Ok to estimate)</span>",
        type: "range",
        value: "$250,001 - 300,000"
    }, {
        name: "TotalMortgageBalance",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "range",
        value: "$50,000 or less"
    }, {
        name: "AnyMortgages",
        actions: [{
            name: "FirstMortgageBalance",
            type: "updateField",
            eventName: "onValidChange",
            setValue: "$50,000 or less",
            triggerRender: !0,
            condition: [{
                name: "AnyMortgages",
                param: "value",
                value: ["No"]
            }
            ]
        }, {
            type: "updateField",
            name: "FirstMortgageBalance",
            eventName: "onValidChange",
            setDynamicValue: !0,
            setAttributes: {
                hidden: !1
            },
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                condition: [{
                    invertComparison: !0,
                    name: "AnyMortgages",
                    param: "value",
                    value: ["No"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "FirstMortgageBalance",
            eventName: "onValidChange",
            setValue: "$150,001 - 175,000",
            triggerRender: !0,
            condition: [{
                name: "PropertyValue",
                param: "value",
                value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                invertComparison: !0,
                condition: [{
                    invertComparison: !0,
                    name: "AnyMortgages",
                    param: "value",
                    value: ["No"]
                }
                ]
            }
            ]
        }, {
            type: "updateField",
            name: "CashOutAmount",
            eventName: "onValidChange",
            setDynamicValue: !0,
            triggerRender: !0,
            condition: [{
                name: "FirstMortgageBalance",
                param: "value",
                value: [null],
                invertComparison: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }
            ]
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 14,
            condition: [{
                name: "FirstMortgageBalance",
                conditionType: "onlyOneAvailableSet",
                condition: [{
                    name: "State",
                    param: "value",
                    value: ["CA"]
                }
                ]
            }
            ]
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 15,
            condition: [{
                name: "FirstMortgageBalance",
                conditionType: "onlyOneAvailableSet",
                condition: [{
                    invertComparison: !0,
                    name: "State",
                    param: "value",
                    value: ["CA"]
                }
                ]
            }
            ]
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 12,
            condition: [{
                name: "AnyMortgages",
                param: "value",
                value: ["First Mortgage Only"]
            }
            ]
        }, {
            type: "jumpOnStep",
            eventName: "onValidChange",
            step: 13,
            condition: [{
                name: "AnyMortgages",
                param: "value",
                value: ["No"]
            }
            ]
        }
        ]
    }, {
        name: "ControlButton",
        actions: [{
            type: "nextStep",
            eventName: "onValidChange"
        }
        ]
    }
    ];
    return Bq.Util.addWhitelabelRules(campaignRules, Bq.wlRules())
}, Bq.CampaignSteps = function() {
    var campaignSteps = [{
        step: 1,
        header: {
            call: "Find low home mortgage rates today!",
            location: "<%= City %>, <%= State %>",
            phone: '<a href="tel:888-511-5538">(888) 511-5538</a>'
        },
        sidebar: {
            hidden: !0
        },
        caption: {
            hidden: !0
        },
        content: {
            fields: [{
                name: "ZipCode",
                validationViewType: !1
            }
            ]
        },
        errors: {
            hidden: !0
        },
        counter: {
            minValue: 1e5,
            minus: 17201968
        },
        action: [{
            type: "popout",
            content: {
                location: "<%= City %>, <%= State %>",
                title: "We found the following Mortgage Refinance Lenders in  <%= City %>",
                subTitle: "Compare additional offers to find the best deal!",
                call: "Lower your monthly payments today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>'
            },
            callback: function() {
                clearInterval(Bq.App.counterStart)
            }
        }
        ]
    }, {
        step: 2,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage With <span class="bq-title-red">HARP</span> Before It Expires In Sept 2017!'
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: [{
                    name: "ZipCode",
                    hidden: !0,
                    hidePrepoped: !0,
                    ignoreHiddenWhenInit: !0
                }, "LoanType", {
                    name: "ControlButton",
                    sets: ["Continue &nbsp;►"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }, {
                        name: "MilitaryOrVeteran",
                        type: "updateField",
                        eventName: "onValidChange",
                        setValue: "No",
                        triggerRender: !0,
                        condition: [{
                            invertComparison: !0,
                            name: "State",
                            param: "value",
                            value: ["CA"]
                        }
                        ]
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-20.gif"
                    }
                    ]
                }
                ]
            }
            ]
        },
        errors: {
            hidden: !1,
            list: !0
        }
    }, {
        step: 3,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>'
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: [{
                    name: "ZipCode",
                    hidden: !0,
                    hidePrepoped: !0,
                    ignoreHiddenWhenInit: !0,
                    template: !1
                }, "PropertyType", {
                    name: "ControlButton",
                    sets: ["Continue &nbsp;►"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-30.gif"
                    }
                    ]
                }
                ]
            }
            ]
        },
        errors: {
            hidden: !1,
            list: !0
        }
    }, {
        step: 4,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "<b> Great News! </b> Low rates found in <%= City || {{your city}} %>!",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["CreditRating"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 5,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Purchase"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 8,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Refinance", "Reverse mortgage"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 6,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Home equity"]
                    }
                    ]
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-40.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 5,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "Potentially save thousands by comparing your mortgage rates!",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: [{
                    name: "PurchaseTimeFrame"
                }
                ]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 8
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-50.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 6,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            subTitle: "What year did you purchase your home?",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["PurchaseYear"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-50.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 7,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["PurchasePrice"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-55.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 8,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "Potentially save thousands by comparing your mortgage rates!",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["PropertyValue"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14,
                    condition: [{
                        name: "FirstMortgageBalance",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Refinance"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 15,
                    condition: [{
                        name: "FirstMortgageBalance",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Refinance"],
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 12,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Refinance"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 9,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Purchase"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14,
                    condition: [{
                        name: "TotalMortgageBalance",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Reverse mortgage"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 15,
                    condition: [{
                        name: "TotalMortgageBalance",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Reverse mortgage"],
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 10,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Reverse mortgage"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 11,
                    condition: [{
                        name: "LoanType",
                        param: "value",
                        value: ["Home equity"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 9
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-60.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 9,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>'
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["DownPayment"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 16
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 10,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["TotalMortgageBalance"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 11,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["AnyMortgages"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    name: "FirstMortgageBalance",
                    type: "updateField",
                    eventName: "onValidChange",
                    setValue: "$50,000 or less",
                    triggerRender: !0,
                    condition: [{
                        name: "AnyMortgages",
                        param: "value",
                        value: ["No"]
                    }
                    ]
                }, {
                    type: "updateField",
                    name: "FirstMortgageBalance",
                    eventName: "onValidChange",
                    setDynamicValue: !0,
                    setAttributes: {
                        hidden: !1
                    },
                    triggerRender: !0,
                    condition: [{
                        name: "PropertyValue",
                        param: "value",
                        value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                        condition: [{
                            invertComparison: !0,
                            name: "AnyMortgages",
                            param: "value",
                            value: ["No"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "updateField",
                    name: "FirstMortgageBalance",
                    eventName: "onValidChange",
                    setValue: "$150,001 - 175,000",
                    triggerRender: !0,
                    condition: [{
                        name: "PropertyValue",
                        param: "value",
                        value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                        invertComparison: !0,
                        condition: [{
                            invertComparison: !0,
                            name: "AnyMortgages",
                            param: "value",
                            value: ["No"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14,
                    condition: [{
                        name: "FirstMortgageBalance",
                        conditionType: "onlyOneAvailableSet"
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 15,
                    condition: [{
                        name: "FirstMortgageBalance",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            invertComparison: !0,
                            name: "State",
                            param: "value",
                            value: ["CA"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 12,
                    condition: [{
                        name: "AnyMortgages",
                        param: "value",
                        value: ["First Mortgage Only"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 13,
                    condition: [{
                        name: "AnyMortgages",
                        param: "value",
                        value: ["No"]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 13
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 12,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["FirstMortgageBalance"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14,
                    condition: [{
                        name: "CashOutAmount",
                        conditionType: "onlyOneAvailableSet"
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 15,
                    condition: [{
                        name: "CashOutAmount",
                        conditionType: "onlyOneAvailableSet",
                        condition: [{
                            invertComparison: !0,
                            name: "State",
                            param: "value",
                            value: ["CA"]
                        }
                        ]
                    }
                    ]
                }, {
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 13
                }, {
                    type: "popup",
                    eventName: "onValidChange",
                    condition: [{
                        name: "FirstMortgageBalance",
                        param: "value",
                        value: ["$50,000 or less"],
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Refinance", "Home equity"],
                            condition: [{
                                conditionType: "popupParams",
                                name: "lowBal",
                                param: "show",
                                value: [!0]
                            }
                            ]
                        }
                        ]
                    }
                    ],
                    dataForView: {
                        className: "bq-MortgageBalance-popup",
                        template: "#bq-MortgageBalance-popup",
                        typeId: ""
                    },
                    dataForModel: {
                        popupName: "lowBal",
                        popupContainerSelector: "body",
                        title: "Great News, It Seems That You Have Almost Paid Off Your Mortgage ",
                        subTitle: "Why not apply for a small personal loan instead of refinancing your current mortgage?",
                        buttons: {
                            learnMore: {
                                link: {
                                    funcName: "redirectLink"
                                },
                                linkText: "Learn More"
                            },
                            call: {
                                phoneNumber: "8886045002",
                                phoneNumberFormated: "(888) 604-5002"
                            }
                        }
                    }
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-70.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 13,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["CashOutAmount"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 14
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-75.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 14,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["InterestRatePercentage"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "jumpOnStep",
                    eventName: "onValidChange",
                    step: 15
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-80.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 15,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["CurrentLoanType", {
                    name: "ControlButton",
                    sets: ["Continue &nbsp;►"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 17
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-85.gif"
                    }
                    ]
                }
                ]
            }
            ]
        },
        errors: {
            list: !1
        }
    }, {
        step: 16,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "Save On Your Mortgage - Get a Quote Today!",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["MilitaryOrVeteran"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-85.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !1
        }
    }, {
        step: 17,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "Your info is safe with us - this form is encrypted with 256 Bit SSL.",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["BirthDate"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-90.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !1
        }
    }, {
        step: 18,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: "Your info is safe with us - this form is encrypted with 256 Bit SSL.",
            subTitle: "Let's finish up. What is your name?",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["FirstName", "LastName"]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-93.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !1
        }
    }, {
        step: 19,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handse"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'Save On Your Mortgage - <span class="bq-title-red"> Get a Quote Today!</span>',
            subTitle: " Great, <%= FirstName %>. What is your address?",
            subTitle2: "We respect your privacy. Your info will only be used to help provide rates.",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["Address", {
                    name: "ZipCode",
                    hidePrepoped: !1,
                    title: "Zip Code:",
                    mark: "<%=City%>, <%=StateCode%>",
                    hidden: !1,
                    template: "#bq-field-zipUS",
                    required: [{
                        type: "notEmpty",
                        message: "Please enter zip code."
                    }, {
                        type: "minLength",
                        value: 4,
                        message: "Enter at least 4 digits."
                    }, {
                        type: "zipUS",
                        message: "Zip Code is invalid."
                    }
                    ]
                }
                ]
            }, {
                name: "ControlButton",
                sets: ["Continue &nbsp;►"],
                actions: [{
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-96.gif"
                }
                ]
            }
            ]
        },
        errors: {
            list: !0
        }
    }, {
        step: 20,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        caption: {
            title: 'You’re almost there! <span class="bq-title-red"> Lenders are preparing your rates.</span>',
            subTitle: "What is the best email and  number to reach you with your quotes?",
            banner: !0
        },
        content: {
            fields: [{
                name: "Markup",
                template: "#bq-row",
                container: ".bq-row-container",
                fields: ["PhoneNumber", "EmailAddress"]
            }, {
                name: "ControlButton",
                sets: ["Get My Quotes!"]
            }, {
                name: "ImageField",
                sets: [{
                    imageURL: "/images/a/progressbar-refi/prog-bar-small-99.gif"
                }
                ]
            }
            ]
        },
        tcpa: !0,
        disclaimer: !0,
        action: "submit",
        errors: {
            list: !0,
            region: !0
        },
        cssClass: "bq-step-submit"
    }, {
        step: 21,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        sidebar: {
            hidden: !0
        },
        caption: {
            title: "Similar offers you may be interested in:"
        },
        content: {
            fields: [{
                name: "ControlButton",
                actions: [{
                    type: "skipUpsell",
                    eventName: "onValidChange"
                }, {
                    type: "nextStep",
                    eventName: "onValidChange"
                }
                ],
                sets: [{
                    label: "Skip",
                    action: "skipUpsell"
                }, {
                    label: "Continue &nbsp;►►",
                    action: "nextStep"
                }
                ]
            }
            ]
        },
        cssClass: "bq-step-upsellPage",
        action: [{
            type: "upsellPage",
            once: !0,
            stopNext: !0
        }, "upsellAction"]
    }, {
        step: 22,
        header: {
            call: "Lower your monthly payments today!",
            phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
            location: "<%= City %>, <%= State %>"
        },
        sidebar: {
            hidden: !0
        },
        caption: {
            title: "Thank you  <%= FirstName %> for submitting your Mortgage Rate request.",
            subTitle: "Compare additional offers to find the best deal!"
        },
        action: [{
            type: "listing",
            parameters: {
                placementposition: "SurehitsPremium"
            }
        }, {
            type: "redirect"
        }
        ],
        GAparam: "thanks",
        cssClass: "bq-step-listings"
    }
    ];
    return Bq.Util.addWhitelabelSteps(campaignSteps, Bq.wlSteps())
}, Bq.CampaignViews = function() {
    return {
        fields: {
            zipUS: {
                updateTitle: function() {
                    var model = this.model.serializeData(), options = Bq.Util.getVarsFromURL();
                    this.$el.find(".bq-label-head").html(Bq.Util.preFillStr(model.subTitle1, options)), this.$el.find(".bq-label-head1").html(Bq.Util.preFillStr(model.subTitle2, options))
                }
            }
        }
    }
}, Bq.CampaignModels = function() {
    return {}
}, Bq.CampaignRouter = function(options) {
    options.nextStepNum < options.currentStepNum && Bq.App.fields.getFields().get("ControlButton").trigger("render")
}, Bq.wlRules = function() {
    return {
        mo: [{
            name: "ZipCode",
            subTitle1: "Instant <%= City %> Mortgage/Refinance Rates",
            subTitle2: "Compare from hundreds of trusted lenders near you!",
            title: "Enter Your Zip Code:",
            insuranceCompany: "RefinanaceCalculator.com",
            hidePrepoped: !1,
            updateLayout: ["update:header", "update:sidebar", "update:caption"],
            template: "#bq-field-firstpage"
        }
        ],
        lo: [{
            name: "ZipCode",
            subTitle1: "Instant <%= City %> Mortgage/Refinance Rates",
            subTitle2: "Compare from hundreds of trusted lenders near you!",
            title: "Enter Your Zip Code:",
            insuranceCompany: "RefinanaceCalculator.com",
            hidePrepoped: !1,
            updateLayout: ["update:header", "update:sidebar", "update:caption"],
            template: "#bq-field-firstpage"
        }
        ],
        rm: [{
            name: "ZipCode",
            subTitle1: "Instant <%= City %> Mortgage/Refinance Rates",
            subTitle2: "Compare from hundreds of trusted lenders near you!",
            title: "Enter Your Zip Code:",
            insuranceCompany: "RefinanaceCalculator.com",
            hidePrepoped: !1,
            updateLayout: ["update:header", "update:sidebar", "update:caption"],
            template: "#bq-field-firstpage"
        }, {
            name: "LoanType",
            value: "Reverse mortgage",
            actions: [{
                name: "CurrentLoanType",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "Not Sure",
                triggerRender: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase"]
                }
                ]
            }, {
                name: "PurchaseTimeFrame",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "0-3 months",
                setAttributes: {
                    hidden: !1
                },
                triggerRender: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase"]
                }
                ]
            }, {
                name: "PurchaseTimeFrame",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "Not Sure",
                setAttributes: {
                    hidden: !0
                },
                triggerRender: !0,
                condition: [{
                    invertComparison: !0,
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase"]
                }
                ]
            }, {
                name: "DownPayment",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "20%",
                triggerRender: !0,
                setAttributes: {
                    hidden: !1
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase"]
                }
                ]
            }, {
                name: "DownPayment",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "20%",
                triggerRender: !0,
                setAttributes: {
                    hidden: !0
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase"],
                    invertComparison: !0
                }
                ]
            }, {
                name: "FirstMortgageBalance",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "$50,000 or less",
                triggerRender: !0,
                setAttributes: {
                    hidden: !0
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase", "Reverse mortgage"]
                }
                ]
            }, {
                name: "FirstMortgageBalance",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "$150,001 - 175,000",
                triggerRender: !0,
                setAttributes: {
                    hidden: !1
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }, {
                name: "TotalMortgageBalance",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "$50,000 or less",
                triggerRender: !0,
                setAttributes: {
                    hidden: !0
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase", "Refinance", "Home equity"]
                }
                ]
            }, {
                name: "TotalMortgageBalance",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "$150,001 - 175,000",
                triggerRender: !0,
                setAttributes: {
                    hidden: !1
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Reverse mortgage"]
                }
                ]
            }, {
                name: "CashOutAmount",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "0 (No cash)",
                triggerRender: !0,
                setAttributes: {
                    hidden: !0
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Purchase", "Reverse mortgage"]
                }
                ]
            }, {
                name: "CashOutAmount",
                type: "updateField",
                eventName: "onValueChange",
                setDynamicValue: !0,
                triggerRender: !0,
                setAttributes: {
                    hidden: !1
                },
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Home equity"]
                }
                ]
            }, {
                name: "PropertyValue",
                type: "updateField",
                eventName: "onValueChange",
                setValue: "$200,001 - 250,000",
                triggerRender: !0,
                condition: [{
                    name: "LoanType",
                    param: "value",
                    value: ["Refinance", "Purchase", "Reverse mortgage", "Home equity"]
                }
                ]
            }, {
                type: "jumpOnStep",
                eventName: "onValidChange",
                step: 4
            }
            ]
        }, {
            name: "PropertyType",
            title: "What type of property are you looking to reverse mortgage?",
            actions: [{
                type: "jumpOnStep",
                eventName: "onValidChange",
                step: 4
            }
            ]
        }
        ]
    }
}, Bq.wlSteps = function() {
    return {
        mo: [{
            step: 1,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:855-774-7574"><i class="bq-footer-handset"></i>(855) 774-7574</a>',
                location: "<%= City %>, <%= State %>"
            },
            action: [{
                type: "popout",
                content: {
                    location: "<%= City %>, <%= State %>",
                    title: "We found the following Mortgage Lenders in <%= City %>",
                    subTitle: "Compare additional offers to find the best deal!",
                    call: "Find low home mortgage rates today!",
                    phone: '<a href="tel:<%= 855-774-7574%>"><i class="bq-footer-handset"></i><%= (855) 774-7574%></a>'
                },
                callback: function() {
                    clearInterval(Bq.App.counterStart)
                }
            }
            ]
        }, {
            step: 2,
            header: {
                call: "Lower your monthly payments today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: 'Save On Your Mortgage With <span class="bq-title-red">HARP</span> Before It Expires In Sept 2017!'
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: [{
                        name: "ZipCode",
                        hidden: !0,
                        hidePrepoped: !0,
                        ignoreHiddenWhenInit: !0,
                        template: "#bq-field-zipUS"
                    }, "LoanType", {
                        name: "ControlButton",
                        sets: ["Continue &nbsp;►"],
                        actions: [{
                            type: "nextStep",
                            eventName: "onValidChange"
                        }, {
                            name: "MilitaryOrVeteran",
                            type: "updateField",
                            eventName: "onValidChange",
                            setValue: "No",
                            triggerRender: !0,
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }, {
                        name: "ImageField",
                        sets: [{
                            imageURL: "/images/a/progressbar-refi/prog-bar-small-24.gif"
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            errors: {
                hidden: !1,
                list: !0
            }
        }
        ],
        lo: [{
            step: 1,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:855-774-7574"><i class="bq-footer-handset"></i>(855) 774-7574</a>',
                location: "<%= City %>, <%= State %>"
            },
            action: [{
                type: "popout",
                content: {
                    location: "<%= City %>, <%= State %>",
                    title: "We found the following Mortgage Lenders in <%= City %>",
                    subTitle: "Compare additional offers to find the best deal!",
                    call: "Find low home mortgage rates today!",
                    phone: '<a href="tel:<%= 855-774-7574%>"><i class="bq-footer-handset"></i><%= (855) 774-7574%></a>'
                },
                callback: function() {
                    clearInterval(Bq.App.counterStart)
                }
            }
            ]
        }, {
            step: 2,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: 'Save On Your Mortgage With <span class="bq-title-red">HARP</span> Before It Expires In Sept 2017!'
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: [{
                        name: "ZipCode",
                        hidden: !0,
                        hidePrepoped: !0,
                        ignoreHiddenWhenInit: !0,
                        template: "#bq-field-zipUS"
                    }, "LoanType", {
                        name: "ControlButton",
                        sets: ["Continue"],
                        actions: [{
                            type: "nextStep",
                            eventName: "onValidChange"
                        }, {
                            name: "MilitaryOrVeteran",
                            type: "updateField",
                            eventName: "onValidChange",
                            setValue: "No",
                            triggerRender: !0,
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }, {
                        name: "ImageField",
                        sets: [{
                            imageURL: "/images/a/progressbar-refi/prog-bar-small-20.gif"
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            errors: {
                hidden: !1,
                list: !0
            }
        }, {
            step: 3,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: [{
                        name: "ZipCode",
                        hidden: !0,
                        hidePrepoped: !0,
                        ignoreHiddenWhenInit: !0,
                        template: !1
                    }, "PropertyType", {
                        name: "ControlButton",
                        sets: ["Continue"],
                        actions: [{
                            type: "nextStep",
                            eventName: "onValidChange"
                        }
                        ]
                    }, {
                        name: "ImageField",
                        sets: [{
                            imageURL: "/images/a/progressbar-refi/prog-bar-small-30.gif"
                        }
                        ]
                    }
                    ]
                }
                ]
            }
        }, {
            step: 4,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["CreditRating"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 5,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Purchase"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 8,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Refinance", "Reverse mortgage"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 6,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Home equity"]
                        }
                        ]
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-40.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 5,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: [{
                        name: "PurchaseTimeFrame"
                    }
                    ]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 8
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-50.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 6,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["PurchaseYear"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-50.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 7,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["PurchasePrice"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-55.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 8,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["PropertyValue"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14,
                        condition: [{
                            name: "FirstMortgageBalance",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                name: "LoanType",
                                param: "value",
                                value: ["Refinance"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15,
                        condition: [{
                            name: "FirstMortgageBalance",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                name: "LoanType",
                                param: "value",
                                value: ["Refinance"],
                                condition: [{
                                    invertComparison: !0,
                                    name: "State",
                                    param: "value",
                                    value: ["CA"]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 12,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Refinance"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 9,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Purchase"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14,
                        condition: [{
                            name: "TotalMortgageBalance",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                name: "LoanType",
                                param: "value",
                                value: ["Reverse mortgage"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15,
                        condition: [{
                            name: "TotalMortgageBalance",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                name: "LoanType",
                                param: "value",
                                value: ["Reverse mortgage"],
                                condition: [{
                                    invertComparison: !0,
                                    name: "State",
                                    param: "value",
                                    value: ["CA"]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 10,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Reverse mortgage"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 11,
                        condition: [{
                            name: "LoanType",
                            param: "value",
                            value: ["Home equity"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 9
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-60.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 9,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["DownPayment"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 16
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 10,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["TotalMortgageBalance"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 11,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["AnyMortgages"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        name: "FirstMortgageBalance",
                        type: "updateField",
                        eventName: "onValidChange",
                        setValue: "$50,000 or less",
                        triggerRender: !0,
                        condition: [{
                            name: "AnyMortgages",
                            param: "value",
                            value: ["No"]
                        }
                        ]
                    }, {
                        type: "updateField",
                        name: "FirstMortgageBalance",
                        eventName: "onValidChange",
                        setDynamicValue: !0,
                        setAttributes: {
                            hidden: !1
                        },
                        triggerRender: !0,
                        condition: [{
                            name: "PropertyValue",
                            param: "value",
                            value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                            condition: [{
                                invertComparison: !0,
                                name: "AnyMortgages",
                                param: "value",
                                value: ["No"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "updateField",
                        name: "FirstMortgageBalance",
                        eventName: "onValidChange",
                        setValue: "$150,001 - 175,000",
                        triggerRender: !0,
                        condition: [{
                            name: "PropertyValue",
                            param: "value",
                            value: ["$50,000 or less", "$50,001 - 75,000", "$75,001 - 100,000", "$100,001 - 125,000", "$125,001 - 150,000", "$150,001 - 175,000"],
                            invertComparison: !0,
                            condition: [{
                                invertComparison: !0,
                                name: "AnyMortgages",
                                param: "value",
                                value: ["No"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14,
                        condition: [{
                            name: "FirstMortgageBalance",
                            conditionType: "onlyOneAvailableSet"
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15,
                        condition: [{
                            name: "FirstMortgageBalance",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 12,
                        condition: [{
                            name: "AnyMortgages",
                            param: "value",
                            value: ["First Mortgage Only"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 13,
                        condition: [{
                            name: "AnyMortgages",
                            param: "value",
                            value: ["No"]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 13
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-65.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 12,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["FirstMortgageBalance"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14,
                        condition: [{
                            name: "CashOutAmount",
                            conditionType: "onlyOneAvailableSet"
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15,
                        condition: [{
                            name: "CashOutAmount",
                            conditionType: "onlyOneAvailableSet",
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 13
                    }, {
                        type: "popup",
                        eventName: "onValidChange",
                        condition: [{
                            name: "FirstMortgageBalance",
                            param: "value",
                            value: ["$50,000 or less"],
                            condition: [{
                                name: "LoanType",
                                param: "value",
                                value: ["Refinance", "Home equity"],
                                condition: [{
                                    conditionType: "popupParams",
                                    name: "lowBal",
                                    param: "show",
                                    value: [!0]
                                }
                                ]
                            }
                            ]
                        }
                        ],
                        dataForView: {
                            className: "bq-MortgageBalance-popup",
                            template: "#bq-MortgageBalance-popup",
                            typeId: ""
                        },
                        dataForModel: {
                            popupName: "lowBal",
                            popupContainerSelector: "body",
                            title: "Great News, It Seems That You Have Almost Paid Off Your Mortgage ",
                            subTitle: "Why not apply for a small personal loan instead of refinancing your current mortgage?",
                            buttons: {
                                learnMore: {
                                    link: {
                                        funcName: "redirectLink"
                                    },
                                    linkText: "Learn More"
                                },
                                call: {
                                    phoneNumber: "8886045002",
                                    phoneNumberFormated: "(888) 604-5002"
                                }
                            }
                        }
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-70.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 13,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["CashOutAmount"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 14
                    }, {
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-75.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 14,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["InterestRatePercentage"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "jumpOnStep",
                        eventName: "onValidChange",
                        step: 15
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-80.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 15,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["CurrentLoanType", {
                        name: "ControlButton",
                        sets: ["Continue"],
                        actions: [{
                            type: "jumpOnStep",
                            eventName: "onValidChange",
                            step: 17
                        }
                        ]
                    }, {
                        name: "ImageField",
                        sets: [{
                            imageURL: "/images/a/progressbar-refi/prog-bar-small-85.gif"
                        }
                        ]
                    }
                    ]
                }
                ]
            }
        }, {
            step: 16,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["MilitaryOrVeteran"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-85.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 17,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["BirthDate"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-90.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 18,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["FirstName", "LastName"]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-93.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 19,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["Address", {
                        name: "ZipCode",
                        hidePrepoped: !1,
                        title: "Zip Code:",
                        mark: "<%=City%>, <%=StateCode%>",
                        hidden: !1,
                        template: "#bq-field-zipUS",
                        required: [{
                            type: "notEmpty",
                            message: "Please enter zip code."
                        }, {
                            type: "minLength",
                            value: 4,
                            message: "Enter at least 4 digits."
                        }, {
                            type: "zipUS",
                            message: "Zip Code is invalid."
                        }
                        ]
                    }
                    ]
                }, {
                    name: "ControlButton",
                    sets: ["Continue"],
                    actions: [{
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-96.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 20,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: ["PhoneNumber", "EmailAddress"]
                }, {
                    name: "ControlButton",
                    sets: ["Get My Quotes!"]
                }, {
                    name: "ImageField",
                    sets: [{
                        imageURL: "/images/a/progressbar-refi/prog-bar-small-99.gif"
                    }
                    ]
                }
                ]
            }
        }, {
            step: 21,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            sidebar: {
                hidden: !0
            },
            caption: {
                title: "Similar offers you may be interested in:"
            },
            content: {
                fields: [{
                    name: "ControlButton",
                    actions: [{
                        type: "skipUpsell",
                        eventName: "onValidChange"
                    }, {
                        type: "nextStep",
                        eventName: "onValidChange"
                    }
                    ],
                    sets: [{
                        label: "Skip",
                        action: "skipUpsell"
                    }, {
                        label: "Continue &nbsp;►►",
                        action: "nextStep"
                    }
                    ]
                }
                ]
            },
            cssClass: "bq-step-upsellPage",
            action: [{
                type: "upsellPage",
                once: !0,
                stopNext: !0
            }, "upsellAction"]
        }, {
            step: 22,
            header: {
                call: "Find low home mortgage rates today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            sidebar: {
                hidden: !0
            },
            caption: {
                title: "Thank you  <%= FirstName %> for submitting your Mortgage Rate request.",
                subTitle: "Compare additional offers to find the best deal!"
            },
            action: [{
                type: "listing",
                parameters: {
                    placementposition: "SurehitsPremium"
                }
            }, {
                type: "redirect"
            }
            ],
            GAparam: "thanks",
            cssClass: "bq-step-listings"
        }
        ],
        rm: [{
            step: 1,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:855-774-7574"><i class="bq-footer-handset"></i>(855) 774-7574</a>',
                location: "<%= City %>, <%= State %>"
            },
            action: [{
                type: "popout",
                content: {
                    location: "<%= City %>, <%= State %>",
                    title: "We found the following Mortgage Lenders in <%= City %>",
                    subTitle: "Compare additional offers to find the best deal!",
                    call: "Find low home mortgage rates today!",
                    phone: '<a href="tel:<%= 855-774-7574%>"><i class="bq-footer-handset"></i><%= (855) 774-7574%></a>'
                },
                callback: function() {
                    clearInterval(Bq.App.counterStart)
                }
            }
            ]
        }, {
            step: 2,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: "Apply for a Reverse Mortgage and Get Extra Cash in Hand"
            },
            content: {
                fields: [{
                    name: "Markup",
                    template: "#bq-row",
                    container: ".bq-row-container",
                    fields: [{
                        name: "ZipCode",
                        hidden: !0,
                        hidePrepoped: !0,
                        ignoreHiddenWhenInit: !0
                    }, "PropertyType", {
                        name: "ControlButton",
                        sets: ["Continue &nbsp;►"],
                        actions: [{
                            type: "jumpOnStep",
                            eventName: "onValidChange",
                            step: 4
                        }, {
                            name: "MilitaryOrVeteran",
                            type: "updateField",
                            eventName: "onValidChange",
                            setValue: "No",
                            triggerRender: !0,
                            condition: [{
                                invertComparison: !0,
                                name: "State",
                                param: "value",
                                value: ["CA"]
                            }
                            ]
                        }
                        ]
                    }, {
                        name: "ImageField",
                        sets: [{
                            imageURL: "/images/a/progressbar-refi/prog-bar-small-36.gif"
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            errors: {
                hidden: !1,
                list: !0
            }
        }, {
            step: 3,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 4,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 5,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 6,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 7,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 8,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: "Get the best rates for your home when you compare!"
            }
        }, {
            step: 9,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 10,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: "Get the best rates for your home when you compare!"
            }
        }, {
            step: 11,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 12,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 13,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 14,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 15,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 16,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 17,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: "Get the best rates for your home when you compare!"
            }
        }, {
            step: 18,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 19,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 20,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 21,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            }
        }, {
            step: 22,
            header: {
                call: "Get an additional source of income from your home!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class=""bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            caption: {
                title: "Thank you  <%= FirstName %> for submitting your Reverse Mortgage Rate request.",
                subTitle: "Compare additional offers to find the best deal!"
            }
        }
        ],
        ct: [{
            step: 1,
            header: {
                call: "Find low home mortgage rates today!",
                location: "<%= City %>, <%= State %>",
                phone: '<a href="tel:888-511-5538">(888) 511-5538</a>'
            },
            sidebar: {
                hidden: !0
            },
            caption: {
                hidden: !0
            },
            content: {
                fields: [{
                    name: "ZipCode",
                    validationViewType: !1
                }
                ]
            },
            errors: {
                hidden: !0
            },
            counter: {
                minValue: 1e5,
                minus: 17201968
            },
            action: [{
                type: "popout",
                content: {
                    location: "<%= City %>, <%= State %>",
                    title: "Thank you <%= FirstName %> for submitting your Mortgage Rate request",
                    call: "Lower your monthly payments today!",
                    phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>'
                },
                callback: function() {
                    clearInterval(Bq.App.counterStart)
                }
            }
            ]
        }, {
            step: 22,
            header: {
                call: "Lower your monthly payments today!",
                phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>',
                location: "<%= City %>, <%= State %>"
            },
            sidebar: {
                hidden: !0
            },
            caption: {
                title: "",
                subTitle: ""
            },
            action: [{
                type: "listing",
                content: {
                    title: "Thank you <%= FirstName %> for submitting your Mortgage Rate request"
                },
                parameters: {
                    placementposition: "SurehitsPremium"
                }
            }, {
                type: "redirect"
            }
            ],
            GAparam: "thanks",
            cssClass: "bq-step-listings"
        }
        ],
        p: [{
            step: 1,
            header: {
                call: 'Welcome Friends of Publishers Clearing House <span class="bq-wlHeader">This offer is not associated with the PCH Sweepstakes. Taking advantage of this offer will not affect your chances of winning the PCH Sweepstakes.</span>',
                location: "<%= City %>, <%= State %>",
                phone: '<a href="tel:888-511-5538">(888) 511-5538</a>'
            },
            sidebar: {
                hidden: !0
            },
            caption: {
                hidden: !0
            },
            content: {
                fields: [{
                    name: "ZipCode",
                    validationViewType: !1
                }
                ]
            },
            errors: {
                hidden: !0
            },
            counter: {
                minValue: 1e5,
                minus: 17201968
            },
            action: [{
                type: "popout",
                content: {
                    location: "<%= City %>, <%= State %>",
                    title: "We found the following Mortgage Refinance Lenders in  <%= City %>",
                    subTitle: "Compare additional offers to find the best deal!",
                    call: "Lower your monthly payments today!",
                    phone: '<a href="tel:<%= inboundPhoneNumber%>"><i class="bq-footer-handset"></i><%= inboundPhoneNumberFormatted%></a>'
                },
                callback: function() {
                    clearInterval(Bq.App.counterStart)
                }
            }
            ]
        }
        ]
    }
}, Bq.App.addInitializer(function() {
    require(["/js/libs/cds2/flipclock.js"], function() {
        Bq.Util.flipClockInit($(".bq-step1 .clock"))
    })
});

