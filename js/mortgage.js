Bq = Bq || {}, Bq.VerticalRules = function() {
    return [{
        name: "FirstName",
        path: "LeadData.ContactData",
        type: "name",
        active: !0,
        title: "First Name:",
        required: [{
            type: "name",
            message: "Please enter valid first name."
        }, {
            type: "minLength",
            value: 2,
            message: "Please enter at least 2 characters."
        }
        ]
    }, {
        name: "LastName",
        path: "LeadData.ContactData",
        type: "name",
        active: !0,
        title: "Last Name:",
        required: [{
            type: "name",
            message: "Please enter valid last name."
        }, {
            type: "minLength",
            value: 2,
            message: "Please enter at least 2 characters."
        }
        ]
    }, {
        name: "PhoneNumber",
        path: "LeadData.ContactData",
        type: "phoneUS",
        active: !0,
        title: "Phone:",
        maxLength: 14,
        required: "Please enter valid phone number.",
        mask: "(999) 999-9999",
        mark: "we respect your privacy"
    }, {
        name: "Address",
        path: "LeadData.ContactData",
        type: "address",
        active: !0,
        title: "Address:",
        required: "Please enter valid street address."
    }, {
        name: "EmailAddress",
        path: "LeadData.ContactData",
        type: "email",
        active: !0,
        title: "Email:",
        required: "Please enter valid email address."
    }, {
        name: "ZipCode",
        path: "LeadData.ContactData",
        type: "zipUS",
        active: !0,
        title: "Zip Code:",
        required: [{
            type: "notEmpty",
            message: "Please enter zip code."
        }, {
            type: "minLength",
            value: 4,
            message: "Enter at least 4 digits."
        }, {
            type: "zipUS",
            message: "Zip Code is invalid."
        }
        ],
        maxLength: 5,
        updateLayout: ["update:header"]
    }, {
        name: "City",
        path: "LeadData.ContactData",
        type: "name",
        active: !0,
        required: "Please enter valid city."
    }, {
        name: "IPAddress",
        path: "LeadData.ContactData",
        type: "ipAddress",
        active: !0,
        required: !0,
        value: "192.168.0.1"
    }, {
        name: "State",
        path: "LeadData.ContactData",
        type: "text",
        active: !0,
        required: "Select a state.",
        initSetsWith: "getStateMap"
    }, {
        name: "BirthDate",
        path: "LeadData.QuoteRequest.Persons.Person",
        type: "composite",
        active: !0,
        title: "Birthdate:",
        required: [{
            type: "composite",
            message: "Select date of birth."
        }, {
            type: "minAge",
            value: 18,
            message: "Select date of birth."
        }
        ],
        collection: [{
            name: "Month",
            type: "select",
            active: !0,
            explicit: "MM"
        }, {
            name: "Day",
            type: "select",
            active: !0,
            explicit: "DD"
        }, {
            name: "Year",
            type: "select",
            active: !0,
            explicit: "YYYY"
        }
        ],
        initSetsWith: ["getSetsYearsRange", - 90, - 18],
        aliasing: !1
    }, {
        name: "CreditRating",
        path: "LeadData.QuoteRequest.Persons.Person",
        type: "radio",
        active: !0,
        title: "Estimate your Credit Rating:",
        required: "Select credit rating.",
        sets: ["Excellent", "Good", "Some Problems", "Major Problems"],
        explicit: "--select--"
    }, {
        name: "PropertyType",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "radio",
        active: !0,
        title: "Property Type:",
        required: "Select property type.",
        sets: ["Single Family Home", "Multi-Family Home", "Condo", "Townhouse", "Mobile Home"],
        explicit: "--select--"
    }, {
        name: "PropertyValue",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        active: !0,
        title: "Estimate your Property Value:",
        required: "Select property value.",
        sets: [{
            label: "$50,000 or less",
            value: "$50,000 or less",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $175,000",
            value: "$150,001 - 175,000",
            validation: 175e3
        }, {
            label: "$175,001 - $200,000",
            value: "$175,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $500,000",
            value: "$450,001 - 500,000",
            validation: 5e5
        }, {
            label: "$500,001 - $750,000",
            value: "$500,001 - 750,000",
            validation: 75e4
        }, {
            label: "$750,001 - $1,000,000",
            value: "$750,001 - 1,000,000",
            validation: 1e6
        }, {
            label: "$1,000,001 - $1,500,000",
            value: "$1,000,001 - 1,500,000",
            validation: 15e5
        }, {
            label: "$1,500,001 - $2,000,000",
            value: "$1,500,001 - 2,000,000",
            validation: 2e6
        }, {
            label: "Over $2,000,000",
            value: "Over $2,000,000",
            validation: 2000001
        }
        ],
        explicit: "--select--"
    }, {
        name: "FirstMortgageBalance",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        active: !0,
        title: "What is left of your mortgage balance?",
        required: [{
            type: "notEmpty",
            message: "What is left of your mortgage balance?"
        }, {
            type: "firstMortgageBalance",
            message: "Mortgage balance must be less than or equal to Property Value."
        }
        ],
        sets: [{
            label: "$50,000 or less",
            value: "$50,000 or less",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $175,000",
            value: "$150,001 - 175,000",
            validation: 175e3
        }, {
            label: "$175,001 - $200,000",
            value: "$175,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $500,000",
            value: "$450,001 - 500,000",
            validation: 5e5
        }, {
            label: "$500,001 - $750,000",
            value: "$500,001 - 750,000",
            validation: 75e4
        }, {
            label: "$750,001 - $1,000,000",
            value: "$750,001 - 1,000,000",
            validation: 1e6
        }, {
            label: "$1,000,001 - $1,500,000",
            value: "$1,000,001 - 1,500,000",
            validation: 15e5
        }, {
            label: "$1,500,001 - $2,000,000",
            value: "$1,500,001 - 2,000,000",
            validation: 2e6
        }, {
            label: "Over $2,000,000",
            value: "Over $2,000,000",
            validation: 2000001
        }
        ],
        explicit: "--select--",
        actions: [{
            name: "CashOutAmount",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: null,
                    setHidden: !0,
                    setValue: null
                }, {
                    notValue: [null],
                    setHidden: !1,
                    setDynamicValue: !0
                }
                ]
            }
        }, {
            type: "hideNotValidSets",
            onValidChange: !0,
            name: "CashOutAmount"
        }
        ]
    }, {
        name: "CashOutAmount",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        active: !0,
        title: "What is your approx. cash out?",
        sets: [{
            label: "0 (No cash)",
            value: "0 (No cash)",
            validation: 0
        }, {
            label: "$1 - $25,000",
            value: "$1 - 25,000",
            validation: 25e3
        }, {
            label: "$25,001 - $50,000",
            value: "$25,001 - 50,000",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $200,000",
            value: "$150,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $700,000",
            value: "$450,001 - 700,000",
            validation: 7e5
        }, {
            label: "$700,001 - $950,000",
            value: "$700,001 - 950,000",
            validation: 95e4
        }, {
            label: "$950,001 - $1,450,000",
            value: "$950,001 - 1,450,000",
            validation: 145e4
        }, {
            label: "$1,450,001 - $1,950,000",
            value: "$1,450,001 - 1,950,000",
            validation: 195e4
        }
        ],
        explicit: "--select--",
        required: [{
            type: "cashOutAmount",
            message: "Cash Out amount must be less than or equal to the difference between Property Value and Mortgage Balance."
        }, {
            type: "notEmpty",
            message: "What is your approx. cash out?"
        }
        ]
    }, {
        name: "AnyBankruptcy",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "radio",
        active: !0,
        title: "Have you had bankruptcy in the last 5 years?",
        sets: ["Yes", "No"],
        required: "Have you had bankruptcy in the last 5 years?",
        explicit: "--select--"
    }, {
        name: "AnyForeclosure",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "radio",
        active: !0,
        title: "Have you had foreclosure in the last 5 years?",
        sets: ["Yes", "No"],
        explicit: "--select--"
    }, {
        name: "BankruptcyTime",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        active: !0,
        hidden: !0,
        title: "How long ago was the bankruptcy?",
        sets: ["Less than 1 year", "1 Year", "2 Years", "3 Years", "4 Years", "5 Years", "More than 5 years"],
        explicit: "--select--",
        required: "How long ago was the bankruptcy?",
        value: "2 Years"
    }, {
        name: "ForeclosureTime",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        hidden: !0,
        active: !0,
        title: "How long ago was the foreclosure?",
        sets: ["Less than 1 year", "1 Year", "2 Years", "3 Years", "4 Years", "5 Years", "More than 5 years"],
        explicit: "--select--",
        required: "How long ago was the foreclosure?"
    }, {
        name: "MilitaryOrVeteran",
        path: "LeadData.QuoteRequest.Persons.Person",
        type: "radio",
        active: !0,
        title: "Are you or your spouse on active duty or a veteran of the U.S. military?",
        explicit: "--select--",
        sets: ["Yes", "No"],
        required: "Are you or your spouse on active duty or a veteran of the U.S. military?"
    }, {
        name: "CurrentLoanType",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "radio",
        active: !0,
        title: "Which of the following loans do you currently have?",
        explicit: "--select--",
        sets: [{
            label: "FHA Loan",
            value: "FHA"
        }, {
            label: "USDA Loan",
            value: "USDA"
        }, {
            label: "VA Loan",
            value: "VA"
        }, {
            label: "Not Sure or Other",
            value: "Not Sure"
        }
        ],
        required: "Which of the following loans do you currently have?",
        value: "Not Sure"
    }, {
        name: "PropertyUse",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        title: "Property use:",
        sets: ["Primary home", "Secondary home", "Rental property"],
        value: "Primary home",
        aliasing: !0
    }, {
        name: "SecondMortgage",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        title: "Second mortgage:",
        sets: ["Yes", "No"],
        value: "No",
        aliasing: !0
    }, {
        name: "LoanType",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        value: "Refinance",
        sets: ["Unspecified", "Purchase", "Refinance", "Home equity", "Reverse mortgage"],
        actions: [{
            name: "PurchaseTimeFrame",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: "Purchase",
                    setHidden: !1,
                    setValue: null
                }, {
                    notValue: "Purchase",
                    setHidden: !0,
                    setValue: "Not Sure"
                }
                ]
            }
        }, {
            name: "DownPayment",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: "Purchase",
                    setHidden: !1,
                    setValue: null
                }, {
                    notValue: "Purchase",
                    setHidden: !0,
                    setValue: "20%"
                }
                ]
            }
        }, {
            name: "FirstMortgageBalance",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: "Purchase",
                    setHidden: !0,
                    setValue: "$50,000 or less"
                }
                ]
            }
        }, {
            name: "CashOutAmount",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: "Purchase",
                    setHidden: !0,
                    setValue: "0 (No cash)"
                }
                ]
            }
        }, {
            name: "PropertyValue",
            type: "updateField",
            onValueChange: {
                values: [{
                    onValue: "Refinance",
                    setValue: null
                }
                ]
            }
        }
        ]
    }, {
        name: "SecondMortgageBalance",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        title: "Second mortgage balance",
        sets: [{
            label: "$50,000 or less",
            value: "$50,000 or less",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $175,000",
            value: "$150,001 - 175,000",
            validation: 175e3
        }, {
            label: "$175,001 - $200,000",
            value: "$175,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $500,000",
            value: "$450,001 - 500,000",
            validation: 5e5
        }, {
            label: "$500,001 - $750,000",
            value: "$500,001 - 750,000",
            validation: 75e4
        }, {
            label: "$750,001 - $1,000,000",
            value: "$750,001 - 1,000,000",
            validation: 1e6
        }, {
            label: "$1,000,001 - $1,500,000",
            value: "$1,000,001 - 1,500,000",
            validation: 15e5
        }, {
            label: "$1,500,001 - $2,000,000",
            value: "$1,500,001 - 2,000,000",
            validation: 2e6
        }, {
            label: "Over $2,000,000",
            value: "Over $2,000,000",
            validation: 2000001
        }
        ],
        value: "$50,000 or less",
        aliasing: !0
    }, {
        name: "PurchaseTimeFrame",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        hidden: !0,
        title: "Purchase Time Frame:",
        sets: ["0-3 months", "3-6 months", "6-12 months", "12+ months", "Not Sure"],
        value: "Not Sure",
        required: "Select Purchase Time Frame."
    }, {
        name: "PurchaseYear",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "number",
        active: !0,
        maxLength: 4,
        title: "What year did you purchase your home?",
        required: [{
            type: "yearSince",
            value: 100
        }, {
            type: "notEmpty",
            message: "What year did you purchase your home?"
        }
        ]
    }, {
        name: "PurchasePrice",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        active: !0,
        title: "How much did you originally purchase your home for?",
        type: "select",
        sets: [{
            label: "$50,000 or less",
            value: "$50,000 or less",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $175,000",
            value: "$150,001 - 175,000",
            validation: 175e3
        }, {
            label: "$175,001 - $200,000",
            value: "$175,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $500,000",
            value: "$450,001 - 500,000",
            validation: 5e5
        }, {
            label: "$500,001 - $750,000",
            value: "$500,001 - 750,000",
            validation: 75e4
        }, {
            label: "$750,001 - $1,000,000",
            value: "$750,001 - 1,000,000",
            validation: 1e6
        }, {
            label: "$1,000,001 - $1,500,000",
            value: "$1,000,001 - 1,500,000",
            validation: 15e5
        }, {
            label: "$1,500,001 - $2,000,000",
            value: "$1,500,001 - 2,000,000",
            validation: 2e6
        }, {
            label: "Over $2,000,000",
            value: "Over $2,000,000",
            validation: 2000001
        }
        ],
        required: [{
            type: "notEmpty",
            message: "What is left of your mortgage balance?"
        }
        ]
    }, {
        name: "TotalMortgageBalance",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        active: !0,
        title: "What is your total mortgage balance?",
        type: "select",
        sets: [{
            label: "$50,000 or less",
            value: "$50,000 or less",
            validation: 5e4
        }, {
            label: "$50,001 - $75,000",
            value: "$50,001 - 75,000",
            validation: 75e3
        }, {
            label: "$75,001 - $100,000",
            value: "$75,001 - 100,000",
            validation: 1e5
        }, {
            label: "$100,001 - $125,000",
            value: "$100,001 - 125,000",
            validation: 125e3
        }, {
            label: "$125,001 - $150,000",
            value: "$125,001 - 150,000",
            validation: 15e4
        }, {
            label: "$150,001 - $175,000",
            value: "$150,001 - 175,000",
            validation: 175e3
        }, {
            label: "$175,001 - $200,000",
            value: "$175,001 - 200,000",
            validation: 2e5
        }, {
            label: "$200,001 - $250,000",
            value: "$200,001 - 250,000",
            validation: 25e4
        }, {
            label: "$250,001 - $300,000",
            value: "$250,001 - 300,000",
            validation: 3e5
        }, {
            label: "$300,001 - $350,000",
            value: "$300,001 - 350,000",
            validation: 35e4
        }, {
            label: "$350,001 - $400,000",
            value: "$350,001 - 400,000",
            validation: 4e5
        }, {
            label: "$400,001 - $450,000",
            value: "$400,001 - 450,000",
            validation: 45e4
        }, {
            label: "$450,001 - $500,000",
            value: "$450,001 - 500,000",
            validation: 5e5
        }, {
            label: "$500,001 - $750,000",
            value: "$500,001 - 750,000",
            validation: 75e4
        }, {
            label: "$750,001 - $1,000,000",
            value: "$750,001 - 1,000,000",
            validation: 1e6
        }, {
            label: "$1,000,001 - $1,500,000",
            value: "$1,000,001 - 1,500,000",
            validation: 15e5
        }, {
            label: "$1,500,001 - $2,000,000",
            value: "$1,500,001 - 2,000,000",
            validation: 2e6
        }, {
            label: "Over $2,000,000",
            value: "Over $2,000,000",
            validation: 2000001
        }
        ],
        required: [{
            type: "notEmpty",
            message: "What is your total mortgage balance?"
        }, {
            type: "firstMortgageBalance",
            message: "Total mortgage balance must be less than or equal to Property Value."
        }
        ]
    }, {
        name: "AnyMortgages",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "radio",
        active: !0,
        title: "Do you have any mortgages?",
        explicit: "--select--",
        sets: [{
            label: "Yes",
            value: "First Mortgage Only"
        }, {
            label: "No",
            value: "No"
        }
        ],
        required: "Do you have any mortgages?"
    }, {
        name: "DownPayment",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        active: !0,
        hidden: !0,
        title: "Approx. down payment:",
        sets: ["5%", "10%", "15%", "20%", "25%", "30%", "40%", "50%", "More than 50%"],
        value: "20%",
        required: "Select your approx. down payment."
    }, {
        name: "ControlButton",
        type: "control",
        template: "#bq-field-button",
        active: !0,
        doNotSend: !0,
        actions: [{
            type: "nextStep",
            onValidChange: !0
        }
        ],
        sets: ["Continue"]
    }, {
        name: "Errors",
        type: "error",
        doNotSend: !0,
        active: !0,
        title: "Please correct the errors above.",
        hidden: !0,
        list: !0,
        errors: [],
        message: ""
    }, {
        name: "BackButton",
        sets: ["<< back"],
        type: "backButton",
        doNotSend: !0,
        active: !0,
        required: !1
    }, {
        name: "TCPAText",
        type: "tcpa",
        active: !0,
        required: !1,
        text: "",
        tcpaWithCheckBox: !1,
        value: "",
        xmlType: "attribute",
        path: "LeadData"
    }, {
        name: "DisclaimerText",
        doNotSend: !0,
        type: "disclaimer",
        active: !0,
        required: !1,
        text: "",
        value: ""
    }, {
        name: "Upsell",
        type: "up",
        active: !0,
        required: !1,
        hidden: !1,
        value: ""
    }, {
        name: "ParagraphText",
        type: "simpleText",
        active: !0,
        doNotSend: !0
    }, {
        name: "ImageField",
        type: "image",
        active: !0,
        doNotSend: !0,
        sets: [{
            imageURL: "",
            hrefURL: ""
        }
        ],
        cssSets: [{
            width: "auto",
            height: "auto"
        }
        ],
        css: {
            height: "100%",
            "text-align": "center"
        }
    }, {
        name: "HeaderText",
        type: "header",
        active: !0,
        doNotSend: !0,
        sets: []
    }, {
        name: "ProgressBar",
        type: "progressBar",
        active: !0,
        doNotSend: !0,
        sets: []
    }, {
        name: "FooterText",
        type: "href",
        active: !0,
        doNotSend: !0,
        sets: []
    }, {
        name: "ImageLogo",
        type: "image",
        active: !0,
        doNotSend: !0,
        sets: [{
            imageURL: "",
            hrefURL: ""
        }
        ],
        cssSets: [{
            width: "auto",
            height: "auto"
        }
        ],
        css: {
            height: "100%"
        }
    }, {
        name: "ImageLine",
        type: "image",
        active: !0,
        doNotSend: !0,
        sets: [{
            imageURL: "",
            hrefURL: ""
        }
        ],
        cssSets: [{
            width: "auto",
            height: "auto"
        }
        ],
        css: {
            "border-top-width": "2px",
            "border-top-style": "solid",
            "border-top-color": "BFC1C0",
            "margin-left": "10px",
            "margin-right": "10px",
            position: "absolute",
            bottom: "80px",
            left: "0",
            right: "0",
            "z-index": "10"
        }
    }, {
        name: "InterestRatePercentage",
        path: "LeadData.QuoteRequest.Mortgages.Mortgage",
        type: "select",
        explicit: "--select--",
        doNotSend: !0,
        active: !0,
        title: "Current Interest Rate",
        sets: [{
            label: "2.00%",
            value: "2.00"
        }, {
            label: "2.50%",
            value: "2.50"
        }, {
            label: "3.00%",
            value: "3.00"
        }, {
            label: "3.50%",
            value: "3.50"
        }, {
            label: "4.00%",
            value: "4.00"
        }, {
            label: "4.50%",
            value: "4.50"
        }, {
            label: "5.00%",
            value: "5.00"
        }, {
            label: "5.50%",
            value: "5.50"
        }, {
            label: "6.00%",
            value: "6.00"
        }, {
            label: "6.50%",
            value: "6.50"
        }, {
            label: "7.00%",
            value: "7.00"
        }, {
            label: "7.50%",
            value: "7.50"
        }, {
            label: "8.00%",
            value: "8.00"
        }, {
            label: "8.50%",
            value: "8.50"
        }, {
            label: "9.00%",
            value: "9.00"
        }, {
            label: "9.50%",
            value: "9.50"
        }, {
            label: "10.00%",
            value: "10.00"
        }, {
            label: "10.50%",
            value: "10.00+"
        }, {
            label: "11.00%",
            value: "10.00+"
        }
        ],
        value: "4.50",
        required: "Current Interest Rate"
    }, {
        name: "SocialSecurityNumber",
        path: "LeadData.QuoteRequest.Persons.Person",
        type: "number",
        active: !1,
        title: "What is your social security number?",
        subTitle: "Your SSN is secure and this will not affect your credit.",
        maxLength: "9",
        required: [{
            type: "minLength",
            value: 9,
            message: "Enter a valid SSN."
        }
        ],
        mandatory: !1
    }, {
        name: "Listings",
        type: "listings",
        active: !0,
        doNotSend: !0
    }, {
        name: "BuyerGuid",
        path: "LeadData.QuoteRequest.SelectedBuyers.SelectedBuyer",
        xmlType: "tag",
        active: !0,
        aliasing: !0,
        doNotSend: !0
    }, {
        name: "BuyerName",
        path: "LeadData.QuoteRequest.SelectedBuyers.SelectedBuyer",
        xmlType: "tag",
        active: !0,
        aliasing: !0,
        doNotSend: !0
    }, {
        name: "QuoteFlow",
        path: "LeadData.QuoteRequest",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !0
    }, {
        name: "QuoteCountry",
        path: "LeadData.QuoteRequest",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !0
    }, {
        name: "QuoteType",
        path: "LeadData.QuoteRequest",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Target",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1,
        value: "Lead.CDSInsert"
    }, {
        name: "RequestTime",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "TimeZone",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "UserAgent",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "SessionLength",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "OriginalURL",
        path: "LeadData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "OfferId",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "SubId",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Sub2Id",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Sub3Id",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Sub4Id",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Sub5Id",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "Source",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "SurveyPath",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "TransactionId",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "LeadId",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "TrustedFormCertUrl",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "VerifyAddress",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1
    }, {
        name: "AffiliateDataId",
        path: "LeadData.AffiliateData",
        xmlType: "attribute",
        active: !0,
        aliasing: !0,
        doNotSend: !1,
        xmlName: "Id",
        multipleXmlName: !0
    }
    ]
};

