			$(function() {

				$('#mobile').mask("999-999-9999");
				//jQuery time
//				var current_fs, next_fs, previous_fs; //fieldsets
//				var left, opacity, scale; //fieldset properties which we will animate
//				var animating; //flag to prevent quick multi-click glitches
//				var is_validated = true;


				//				$(".submit").click(function() {
				$('#smart-form').submit(function() {





     
//        // show that something is loading
//        $('#response').html("<b>Loading response...</b>");
//         
//        /*
//         * 'post_receiver.php' - where you will pass the form data
//         * $(this).serialize() - to easily read form data
//         * function(data){... - data contains the response from post_receiver.php
//         */
//        $.post('post_receiver.php', $(this).serialize(), function(data){
//             
//            // show the response
//            $('#response').html(data);
//             
//        }).fail(function() {
//         
//            // just in case posting your form failed
//            alert( "Posting failed." );
//             
//        });
// 
//        // to prevent refreshing the whole page page
//        return false;
// 
//
//});


					is_validated = $("#smart-form").valid();

					if (is_validated) {

						var url = "post.php";
						var sData = $(this).serialize();
						$.ajax({
							url: url,
							type: 'POST',
							data: sData,
							cache: false,
							beforeSend: function(xhr) {
								// console.log('before send, validate everything');
								$('#loader').show('slow');
							},
							success: function(response, code, jqXHR) {
								// console.log(response, response.result);
								//													var resp =  JSON.parse(response);
								var resp = JSON.parse(response);
								// console.log(resp, resp.result)
								
								if (resp.result == "success") {

									//redirect url
									$('#loader').show('slow');
									
									//									var responseUrl = resp.redirect_url.substring(1, resp.redirect_url.length-1);
									var responseUrl = resp.redirect_url;
									window.location = responseUrl;
									//									return false;
								} else {
									//REDIRECT IF ERROR :D
									window.location = "https://www.yahoo.com";
									//alert(resp.msg)
								}
							},
							error: function(xhr, exception) {
								console.log('error');
								$('#loader').hide('slow');
							}
						});
						return false;
					}

				});



			});

// Form validation

			$("#smart-form").validate({

				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",

				rules: {
					first_name: {
						required: true
					},
					last_name: {
						required: true
					},
					email: {
						required: true,
						email: true
					},
					mobile: {
						required: true
					},

					PurchaseYear: {
						required: true
					},

					languages: {
						required: true
					}

				},
				// end rules
				messages: {
					
					month: {
						required: 'Please select the month you were born'
					},
					
					day: {
						required: 'Please select the day you were born'
					},
					
					year: {
						required: 'Please select the year you were born'
					},
					
					first_name: {
						required: 'Enter your first name'
					},
					last_name: {
						required: 'Enter your last name'
					},
					email: {
						required: 'Enter your email address',
						email: 'Enter a VALID email address'
					},
					mobile: {
						required: 'Please enter a valid phone number'
					},
					PurchaseYear: {
						required: 'What year did you puchase your home?'
					},
					languages: {
						required: 'Select laguages spoken'
					}

				},
				// end messages
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				// end highlight
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				// end unhighlight
				errorPlacement: function(error, element) {
					if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
					} else {
						error.insertAfter(element.parent());
					}

				} // end errorPlacement															
			}); // end validate
			