<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Mortgage Refinance Quote</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/smart-forms.css">
		<!--<link rel="stylesheet" type="text/css" href="css/custom.css">-->
		<link rel="stylesheet" type="text/css" href="css/smart-addons.css">
		<link rel="stylesheet" type="text/css" href="css/smart-themes/red.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
		<!--<link rel="stylesheet" href="css/normalize.css" />-->
		<link rel="stylesheet" href="css/ion.rangeSlider.css" />
		<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
		<script type="text/javascript" src="js/jquery.steps.js"></script>
		<script type="text/javascript" src="js/jquery-ui-custom.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-slider-pips.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.formShowHide.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-monthpicker.min.js"></script>
		<script type="text/javascript" src="js/jquery.validate.min.js"></script>
		<!-- <script type="text/javascript" src="js/jquery.maskedinput.js"></script>-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		<script type="text/javascript" src="js/smart-form.js"></script>
		<script src="js/ion.rangeSlider.js"></script>
		<script type="text/javascript">
			$(function(){ 
			  	$('.smartfm-ctrl').formShowHide(); 
			});
		</script>
		<!--[if lte IE 8]>
			<link type="text/css" rel="stylesheet" href="css/smart-forms-ie8.css">
		<![endif]-->
		<style>
			.irs {
				height: 55px;
			}
			.irs-with-grid {
				height: 75px;
			}
			.irs-line {
				height: 10px; top: 33px;
				background: #EEE;
				background: linear-gradient(to bottom, #DDD -50%, #FFF 150%); /* W3C */
				border: 1px solid #CCC;
				border-radius: 16px;
				-moz-border-radius: 16px;
			}
				.irs-line-left {
					height: 8px;
				}
				.irs-line-mid {
					height: 8px;
				}
				.irs-line-right {
					height: 8px;
				}
			
			.irs-bar {
				height: 10px; top: 33px;
				border-top: 1px solid #428bca;
				border-bottom: 1px solid #428bca;
				background: #428bca;
				background: linear-gradient(to top, rgba(66,139,202,1) 0%,rgba(127,195,232,1) 100%); /* W3C */
			}
				.irs-bar-edge {
					height: 10px; top: 33px;
					width: 14px;
					border: 1px solid #428bca;
					border-right: 0;
					background: #428bca;
					background: linear-gradient(to top, rgba(66,139,202,1) 0%,rgba(127,195,232,1) 100%); /* W3C */
					border-radius: 16px 0 0 16px;
					-moz-border-radius: 16px 0 0 16px;
				}
			
			.irs-shadow {
				height: 2px; top: 38px;
				background: #000;
				opacity: 0.3;
				border-radius: 5px;
				-moz-border-radius: 5px;
			}
			.lt-ie9 .irs-shadow {
				filter: alpha(opacity=30);
			}
			
			.irs-slider {
				top: 25px;
				width: 27px; height: 27px;
				border: 1px solid #AAA;
				background: #DDD;
				background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(220,220,220,1) 20%,rgba(255,255,255,1) 100%); /* W3C */
				border-radius: 27px;
				-moz-border-radius: 27px;
				box-shadow: 1px 1px 3px rgba(0,0,0,0.3);
				cursor: pointer;
			}
			
			.irs-slider.state_hover, .irs-slider:hover {
				background: #FFF;
			}
			
			.irs-min, .irs-max {
				color: #333;
				font-size: 12px; line-height: 1.333;
				text-shadow: none;
				top: 0;
				padding: 1px 5px;
				background: rgba(0,0,0,0.1);
				border-radius: 3px;
				-moz-border-radius: 3px;
			}
			
			.lt-ie9 .irs-min, .lt-ie9 .irs-max {
				background: #ccc;
			}
			
			.irs-from, .irs-to, .irs-single {
				color: #fff;
				font-size: 14px; line-height: 1.333;
				text-shadow: none;
				padding: 1px 5px;
				background: #428bca;
				border-radius: 3px;
				-moz-border-radius: 3px;
			}
			.lt-ie9 .irs-from, .lt-ie9 .irs-to, .lt-ie9 .irs-single {
				background: #999;
			}
			
			.irs-grid {
				height: 27px;
			}
			.irs-grid-pol {
				opacity: 0.5;
				background: #428bca;
			}
			.irs-grid-pol.small {
				background: #999;
			}
			
			.irs-grid-text {
				display: none;
				bottom: 5px;
				color: #99a4ac;
			}
			
			.irs-grid-pol.small {
				display: none;
				height: 4px;
			}
			
			.irs-disabled {
			}

			.wizard .steps {
				display:none;
			}
			.wizard .actions {
				display:none;
			}
		</style>
	</head>
	<body>
		<div class="smart-wrap">
			<div class="smart-forms smart-container smart-flat wrap-1">
				<div class="form-body stp-three theme-red steps-theme-red">
					<form id="smart-form" action="" method="post" enctype="multipart/form-data" />
					<input type="hidden" name="lp_s1" value="<?php echo $_GET['subid']?>" />
					<input type="hidden" name="lp_s2" value="<?php echo $_GET['c1']?>" />
					<input type="hidden" name="lp_s3" value="<?php echo $_GET['c2']?>" />
					<input type="hidden" name="lp_s4" value="<?php echo $_GET['c3']?>" />
					<input type="hidden" name="lp_request_id" value="<?php echo $_GET['requestid']?>" />
					<input type="hidden" name="ip_address" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
					<h2>First Step</h2>
					<fieldset id="contact-info-fieldset">
						<label for="LoanType" class="questions">Enter your Zip Code</label>
						<div class="frm-row">
							<label class="field prepend-icon">
								<input type="text" name="zip_code" id="zip_code" class="gui-input" placeholder="ZIP CODE" required>
							</label>
						</div>
						<div class="bottom">
							<center>
								<button type="button" class="next-step" id="zipCode">NEXT</button>
							</center>
						</div>

					</fieldset>


					<h2>Step 1 (Loan Type)</h2>
					<fieldset id="LoanTypeFieldset" class="spacer-b30">
						<label for="LoanType" class="questions">What type of home loan are you looking for?</label>
						<div class="frm-row">
							<div class="colm colm3 inner-center">
								<div class="score-card">Refinance</div>
								<label class="rad">
									<input type="radio" name="LoanType" value="Refinance" id="Refinance">
									<img src="images/blank.png" class="loan-for-refi fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Purchase</div>
								<label class="rad">
									<input type="radio" name="LoanType" value="Purchase" id="Purchase">
									<img src="images/blank.png" class="loan-for-purchase fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Reverse Mortgage</div>
								<label class="rad">
									<input type="radio" name="LoanType" value="Reverse mortgage" id="Reverse">
									<img src="images/blank.png" class="loan-for-rm fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Home Equity</div>
								<label class="rad">
									<input type="radio" name="LoanType" value="Home equity" id="HomeEquity">
									<img src="images/blank.png" class="loan-for-he fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 2 (Property Type)</h2>
					<fieldset id="PropertyTypeFieldset" class="spacer-b30">
						<label for="PropertyType" class="questions">What type of property is this loan for?</label>
						<div class="frm-row">
							<div class="colm colm3 inner-center">
								<div class="score-card">Single Family</div>
								<label class="rad">
									<input type="radio" name="PropertyType" value="Single family home">
									<img src="images/blank.png" class="type-single-family fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Multi-Family</div>
								<label class="rad">
									<input type="radio" name="PropertyType" value="Multi-family home">
									<img src="images/blank.png" class="type-multi-family fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Condo</div>
								<label class="rad">
									<input type="radio" name="PropertyType" value="Condo">
									<img src="images/blank.png" class="type-condo fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Townhouse</div>
								<label class="rad">
									<input type="radio" name="PropertyType" value="Townhouse">
									<img src="images/blank.png" class="type-townhouse fade">
								</label>
							</div>
							<!--<div class="colm colm3 inner-center">
									<div class="score-card">Single Family</div>
								<label class="rad">
									<input type="radio" name="PropertyType" value="Mobile home">
									<img src="http://placehold.it/150x40/35d/fff&text=Mobile home">
								</label>
								</div>--></div>
					</fieldset>
					<h2>Step 2.1 (Property Use)</h2>
					<fieldset id="PropertyUseFieldset" class="spacer-b30">
						<label for="PropertyUse" class="questions">How will this property be used?</label>
						<div class="frm-row">
						<div class="colm colm4 inner-center">
							<div class="score-card">Primary home</div>
								<label class="rad">
									<input type="radio" name="PropertyUse" value="Primary home">
									<img src="http://placehold.it/150x40/35d/fff&text=Primary home">
								</label>
								</div>
								<div class="colm colm4 inner-center">
									<div class="score-card">Secondary home</div>
								<label class="rad">
									<input type="radio" name="PropertyUse" value="Secondary home">
									<img src="http://placehold.it/150x40/35d/fff&text=Secondary home">
								</label>
								</div>
								<div class="colm colm4 inner-center">
									<div class="score-card">Rental property</div>
								<label class="rad">
									<input type="radio" name="PropertyUse" value="Rental property">
									<img src="http://placehold.it/150x40/35d/fff&text=Rental property">
								</label>
								</div>
						</div>
					</fieldset>
					<h2>Step 3 (Credit Rating)</h2>
					<fieldset id="CreditRatingFieldset" class="spacer-b30">
						<label for="CreditRating" class="questions">Estimate your credit score:
							<br>(Most people have Good credit)</label>
						<div class="frm-row">
							<div class="colm colm3 inner-center">
								<div class="score-card">Excellent (720+)</div>
								<label class="rad">
									<input type="radio" name="CreditRating" value="Excellent">
									<img src="images/blank.png" class="excellent-credit fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Good (680-719)</div>
								<label class="rad">
									<input type="radio" name="CreditRating" value="Good">
									<img src="images/blank.png" class="good-credit fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Fair (640-679)</div>
								<label class="rad">
									<input type="radio" name="CreditRating" value="Some Problems">
									<img src="images/blank.png" class="problem-credit fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Poor (Below 619)</div>
								<label class="rad">
									<input type="radio" name="CreditRating" value="Poor">
									<img src="images/blank.png" class="poor-credit fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 3.1 (Purchase Time Frame)</h2>
					<fieldset id="PurchaseTimeFrameFieldset" class="spacer-b30">
						<label for="PurchaseTimeFrame" class="questions">Great, when do you plan on purchasing your home?</label>
						<div class="frm-row">
							<div class="colm colm3 inner-center">
								<div class="score-card">0-3 months</div>
								<label class="rad">
									<input type="radio" name="PurchaseTimeFrame" value="0-3 months">
									<img src="images/blank.png" class="ptf-opt1 fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">3-6 months</div>
								<label class="rad">
									<input type="radio" name="PurchaseTimeFrame" value="3-6 months">
									<img src="images/blank.png" class="ptf-opt2 fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">6-12 months</div>
								<label class="rad">
									<input type="radio" name="PurchaseTimeFrame" value="6-12 months">
									<img src="images/blank.png" class="ptf-opt3 fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">12+ months</div>
								<label class="rad">
									<input type="radio" name="PurchaseTimeFrame" value="12+ months">
									<img src="images/blank.png" class="ptf-opt4 fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 3.2 (Purchase Year)</h2>
					<fieldset id="PurchaseYearFieldset" class="spacer-b30">
						<label for="PurchaseYear" class="questions">What year did you purchase your home?</label>
						<div class="frm-row">
							<div class="colm colm5"></div>
							<div class="colm colm2 inner-center">
								<label class="field prepend-icon">
									<input type="tel" value="" maxlength="4" name="PurchaseYear" class="gui-input" id="month-picker" required>
									<span class="field-icon">
										<i class="fa fa-calendar-o"></i>
									</span>
								</label>
							</div>
							<div class="colm colm5"></div>
						</div>
					</fieldset>
					<h2>Step 3.3 (Purchase Price)</h2>
					<fieldset id="PurchasePriceFieldset" class="spacer-b30">
						<label for="PurchasePrice" class="questions">How much did you purchase your home for?</label>
						<div style="padding: 0px;">
							<input id="PurchasePrice" name="PurchasePrice" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 3.4 (First Time Buyer)</h2>
					<!-- Show only for Home Equity-->
					<fieldset id="FirstTimeBuyerFieldset" class="spacer-b30">
						<label for="FirstTimeBuyerFieldset" class="questions">Are you a first time buyer?</label>
						<div class="frm-row">
							<div class="colm colm6 inner-center">
								<div class="score-card">Yes</div>
								<label class="rad">
									<input type="radio" name="FirstTimeBuyer" value="Yes">
									<img src="images/blank.png" class="yes-mortgage fade">
								</label>
							</div>
							<div class="colm colm6 inner-center">
								<div class="score-card">No</div>
								<label class="rad">
									<input type="radio" name="FirstTimeBuyer" value="No">
									<img src="images/blank.png" class="no-mortgage fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 3.5 (Any Bankruptcy)</h2>
					<!-- Required -->
					<fieldset id="AnyBankruptcyFieldset" class="spacer-b30">
						<label for="AnyBankruptcy" class="questions">Do you have any bankruptcies?</label>
						<div class="frm-row">
							<div class="colm colm6 inner-center">
								<div class="score-card">Yes</div>
								<label class="rad">
									<input type="radio" name="AnyBankruptcy" value="Yes">
									<img src="images/blank.png" class="yes-mortgage fade">
								</label>
							</div>
							<div class="colm colm6 inner-center">
								<div class="score-card">No</div>
								<label class="rad">
									<input type="radio" name="AnyBankruptcy" value="No">
									<img src="images/blank.png" class="no-mortgage fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 3.5.1 (Bankruptcy Time)</h2>
					<!--Show if Bankruptcy Yes-->
					<fieldset id="BankruptcyTimeFieldset" class="spacer-b30">
						<label for="BankruptcyTime" class="questions">How long ago was your bankruptcy:</label>
						<div style="padding: 0px;">
							<input id="BankruptcyTime" name="BankruptcyTime" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 3.6 (Any Foreclosure)</h2>
					<!-- Show only for Home Equity-->
					<fieldset id="AnyForeclosureFieldset" class="spacer-b30">
						<label for="AnyForeclosure" class="questions">Do you have any foreclosures?</label>
						<div class="frm-row">
							<div class="colm colm6 inner-center">
								<div class="score-card">Yes</div>
								<label class="rad">
									<input type="radio" name="AnyForeclosure" value="Yes">
									<img src="images/blank.png" class="yes-mortgage fade">
								</label>
							</div>
							<div class="colm colm6 inner-center">
								<div class="score-card">No</div>
								<label class="rad">
									<input type="radio" name="AnyForeclosure" value="No">
									<img src="images/blank.png" class="no-mortgage fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 3.7 (Mortgages)</h2>
					<!-- Show only for Home Equity-->
					<fieldset id="AnyMortgagesFieldset" class="spacer-b30">
						<label for="AnyMortgages" class="questions">Do you have any mortgages?</label>
						<div class="frm-row">
							<div class="colm colm6 inner-center">
								<div class="score-card">Yes</div>
								<label class="rad">
									<input type="radio" name="AnyMortgages" value="Yes">
									<img src="images/blank.png" class="yes-mortgage fade">
								</label>
							</div>
							<div class="colm colm6 inner-center">
								<div class="score-card">No</div>
								<label class="rad">
									<input type="radio" name="AnyMortgages" value="No">
									<img src="images/blank.png" class="no-mortgage fade">
								</label>
							</div>
						</div>
					</fieldset>

					<h2>Step 4 (Property Value)</h2>
					<fieldset id="PropertyValueFieldset" class="spacer-b30">
						<label for="PropertyValue" class="questions">Estimate your property value:</label>
						<div style="padding: 0px;">
							<input id="PropertyValue" name="PropertyValue" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 5 (First Mortgage Balance)</h2>
					<!--Show only for Refinance and Home Refinance option-->
					<fieldset id="FirstMortgageBalanceFieldset" class="spacer-b30">
						<label for="FirstMortgageBalance" class="questions">What is the remaining balance on your mortgage?</label>
						<div style="padding: 0px;">
							<input id="FirstMortgageBalance" name="FirstMortgageBalance" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 5.1 (Downpayment)</h2>
					<!--Show only for Purchase option-->
					<fieldset id="DownPaymentFieldset" class="spacer-b30" class="DownPaymentFieldset">
						<label for="DownPayment" class="questions">How much can you put down as down payment?</label>
						<div style="padding: 0px;">
							<input id="DownPayment" name="DownPayment" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 5.2 (Total Mortgage Balance)</h2>
					<!--Show only for Refinance and Home Refinance option-->
					<fieldset id="TotalMortgageBalanceFieldset" class="spacer-b30">
						<label for="TotalMortgageBalance" class="questions">What is the total balance on your mortgage?</label>
						<div style="padding: 0px;">
							<input id="TotalMortgageBalance" name="TotalMortgageBalance" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 6 (Cash Out Amount)</h2>
					<!--Hide for Purchase and Reverse Mortgage options-->
					<fieldset id="CashOutAmountFieldset" class="spacer-b30">
						<label for="CashOutAmount" class="questions">Would you like to borrow additional cash?</label>
						<div style="padding: 0px;">
							<input id="CashOutAmount" name="CashOutAmount" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 7 (Current Interest Rate)</h2>
					<!--Hide for Purchase option-->
					<fieldset id="InterestRatePercentageFieldset" class="spacer-b30">
						<label for="InterestRatePercentage" class="questions">What is your current rate on your mortgage?</label>
						<div style="padding: 0px;">
							<input id="InterestRatePercentage" name="InterestRatePercentage" readonly="" class="irs-hidden-input">
						</div>
					</fieldset>
					<h2>Step 8 (Current Loan Type)</h2>
					<!--Hide for Purchase option-->
					<fieldset id="CurrentLoanTypeFieldset" class="spacer-b30">
						<label for="CurrentLoanType" class="questions">Which of the following loans do you currently have?</label>
						<div class="frm-row">
							<div class="colm colm3 inner-center">
								<div class="score-card">FHA Loan</div>
								<label class="rad">
									<input type="radio" name="CurrentLoanType" value="FHA">
									<img src="images/blank.png" class="loantype-fha fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">USDA Loan</div>
								<label class="rad">
									<input type="radio" name="CurrentLoanType" value="USDA">
									<img src="images/blank.png" class="loantype-usda fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">VA Loan</div>
								<label class="rad">
									<input type="radio" name="CurrentLoanType" value="VA">
									<img src="images/blank.png" class="loantype-va fade">
								</label>
							</div>
							<div class="colm colm3 inner-center">
								<div class="score-card">Not Sure or Other</div>
								<label class="rad">
									<input type="radio" name="CurrentLoanType" value="Not Sure ">
									<img src="images/blank.png" class="loantype-other fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>8 (Military Or Veteran)</h2>
					<!-- Show only for Purchase Option-->
					<fieldset id="AnyMortgagesFieldset" class="spacer-b30">
						<label for="MilitaryOrVeteran" class="questions">Have you or your spouse served in the military?</label>
						<div class="frm-row">
							<div class="colm colm6 inner-center">
								<div class="score-card">Yes</div>
								<label class="rad">
									<input type="radio" name="MilitaryOrVeteran" value="Yes">
									<img src="images/blank.png" class="yes-mortgage fade">
								</label>
							</div>
							<div class="colm colm6 inner-center">
								<div class="score-card">No</div>
								<label class="rad">
									<input type="radio" name="MilitaryOrVeteran" value="No">
									<img src="images/blank.png" class="no-mortgage fade">
								</label>
							</div>
						</div>
					</fieldset>
					<h2>Step 9 (Date of Birth)</h2>
					<fieldset id="BirthDateFieldset" class="spacer-b30">
						<label for="BirthDate" class="questions">When were you born?</label>
						<div class="section">
							<div class="frm-row">
								<div class="section colm colm4">
									<label for="month" class="field select">
										<select data-name="Month" name="month" required>
											<option value="">Month</option>
											<option value="01">Jan</option>
											<option value="02">Feb</option>
											<option value="03">Mar</option>
											<option value="04">Apr</option>
											<option value="05">May</option>
											<option value="06">Jun</option>
											<option value="07">Jul</option>
											<option value="08">Aug</option>
											<option value="09">Sep</option>
											<option value="10">Oct</option>
											<option value="11">Nov</option>
											<option value="12">Dec</option>
										</select>
										<i class="arrow double"></i>
									</label>
								</div>
								<div class="section colm colm4">
									<label for="day" class="field select">
										<select data-name="Day" name="day" required>
											<option value="">Day</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31`">31</option>
										</select>
										<i class="arrow double"></i>
									</label>
								</div>
								<div class="section colm colm4">
									<label for="year" class="field select">
										<select data-name="Year" name="year" required>
											<option value="">Year</option>
											<option value="1999">1999</option>
											<option value="1998">1998</option>
											<option value="1997">1997</option>
											<option value="1996">1996</option>
											<option value="1995">1995</option>
											<option value="1994" selected="">1994</option>
											<option value="1993">1993</option>
											<option value="1992">1992</option>
											<option value="1991">1991</option>
											<option value="1990">1990</option>
											<option value="1989">1989</option>
											<option value="1988">1988</option>
											<option value="1987">1987</option>
											<option value="1986">1986</option>
											<option value="1985">1985</option>
											<option value="1984">1984</option>
											<option value="1983">1983</option>
											<option value="1982">1982</option>
											<option value="1981">1981</option>
											<option value="1980">1980</option>
											<option value="1979">1979</option>
											<option value="1978">1978</option>
											<option value="1977">1977</option>
											<option value="1976">1976</option>
											<option value="1975">1975</option>
											<option value="1974">1974</option>
											<option value="1973">1973</option>
											<option value="1972">1972</option>
											<option value="1971">1971</option>
											<option value="1970">1970</option>
											<option value="1969">1969</option>
											<option value="1968">1968</option>
											<option value="1967">1967</option>
											<option value="1966">1966</option>
											<option value="1965">1965</option>
											<option value="1964">1964</option>
											<option value="1963">1963</option>
											<option value="1962">1962</option>
											<option value="1961">1961</option>
											<option value="1960">1960</option>
											<option value="1959">1959</option>
											<option value="1958">1958</option>
											<option value="1957">1957</option>
											<option value="1956">1956</option>
											<option value="1955">1955</option>
											<option value="1954">1954</option>
											<option value="1953">1953</option>
											<option value="1952">1952</option>
											<option value="1951">1951</option>
											<option value="1950">1950</option>
											<option value="1949">1949</option>
											<option value="1948">1948</option>
											<option value="1947">1947</option>
											<option value="1946">1946</option>
											<option value="1945">1945</option>
											<option value="1944">1944</option>
											<option value="1943">1943</option>
											<option value="1942">1942</option>
											<option value="1941">1941</option>
											<option value="1940">1940</option>
											<option value="1939">1939</option>
											<option value="1938">1938</option>
											<option value="1937">1937</option>
											<option value="1936">1936</option>
											<option value="1935">1935</option>
											<option value="1934">1934</option>
											<option value="1933">1933</option>
											<option value="1932">1932</option>
											<option value="1931">1931</option>
											<option value="1930">1930</option>
											<option value="1929">1929</option>
											<option value="1928">1928</option>
											<option value="1927">1927</option>
										</select>
										<i class="arrow double"></i>
									</label>
								</div>
							</div>
						</div>
					</fieldset>
					<h2>Final Step</h2>
					<fieldset id="contact-info-fieldset">
						<div class="spacer-b30">
							<h2 class="questions" style="margin-top: 0px;">
								<i class="fa fa-check" aria-hidden="true"></i>Almost done!</h2>
							<h3>Complete to see your options now.</h3>
							<!-- .tagline -->
						</div>
						<div class="section">
							<label class="field prepend-icon">
								<input type="text" name="first_name" id="first_name" class="gui-input" placeholder="First name..." required>
								<span class="field-icon">
									<i class="fa fa-user"></i>
								</span>
							</label>
						</div>
						<!-- end section -->
						<div class="section">
							<label class="field prepend-icon">
								<input type="text" name="last_name" id="last_name" class="gui-input" placeholder="Last name..." required>
								<span class="field-icon">
									<i class="fa fa-user"></i>
								</span>
							</label>
						</div>
						<!-- end section -->
						<div class="section">
							<label class="field prepend-icon">
								<input type="email" name="email_address" id="email_address" class="gui-input" placeholder="Email address" required>
								<span class="field-icon">
									<i class="fa fa-envelope"></i>
								</span>
							</label>
						</div>
						<!-- end section -->
						<div class="section">
							<label for="mobile" class="field prepend-icon">
								<input type="tel" name="mobile" id="mobile" class="gui-input" placeholder="(Phone) XXX-XXX-XXXX" required>
								<span class="field-icon">
									<i class="fa fa-phone-square"></i>
								</span>
							</label>
						</div>
						<!-- end section -->
						<div class="section">
							<label class="field prepend-icon">
								<input type="text" name="address" id="address" class="gui-input" placeholder="Address" required>
								<span class="field-icon">
									<i class="fa fa-envelope"></i>
								</span>
							</label>
						</div>
						<div class="frm-row">
						
						    <div class="section colm colm3">
						        <label class="field prepend-icon">
						            <input type="text" name="zip" id="zip" class="gui-input" placeholder="Zip" required>
						            <span class="field-icon"><i class="fa fa-certificate"></i></span> 
						        </label>
						    </div><!-- end section -->
						    
						    <div class="section colm colm4">
						        <label class="field prepend-icon">
						            <input type="text" name="city" id="city" class="gui-input" placeholder="City" required> 
						            <span class="field-icon"><i class="fa fa-building-o"></i></span> 
						        </label>
						    </div><!-- end section -->
						    
						    <div class="section colm colm5">
						        <label class="field select">
						            <select id="states" name="state" required>
						                <option value="">Choose state</option>
						                <option value="AL">Alabama</option>
						                <option value="AK">Alaska</option>
						                <option value="AZ">Arizona</option>
						                <option value="AR">Arkansas</option>
						                <option value="CA">California</option>
						                <option value="CO">Colorado</option>
						                <option value="CT">Connecticut</option>
						                <option value="DE">Delaware</option>
						                <option value="DC">District Of Columbia</option>
						                <option value="FL">Florida</option>
						                <option value="GA">Georgia</option>
						                <option value="HI">Hawaii</option>
						                <option value="ID">Idaho</option>
						                <option value="IL">Illinois</option>
						                <option value="IN">Indiana</option>
						                <option value="IA">Iowa</option>
						                <option value="KS">Kansas</option>
						                <option value="KY">Kentucky</option>
						                <option value="LA">Louisiana</option>
						                <option value="ME">Maine</option>
						                <option value="MD">Maryland</option>
						                <option value="MA">Massachusetts</option>
						                <option value="MI">Michigan</option>
						                <option value="MN">Minnesota</option>
						                <option value="MS">Mississippi</option>
						                <option value="MO">Missouri</option>
						                <option value="MT">Montana</option>
						                <option value="NE">Nebraska</option>
						                <option value="NV">Nevada</option>
						                <option value="NH">New Hampshire</option>
						                <option value="NJ">New Jersey</option>
						                <option value="NM">New Mexico</option>
						                <option value="NY">New York</option>
						                <option value="NC">North Carolina</option>
						                <option value="ND">North Dakota</option>
						                <option value="OH">Ohio</option>
						                <option value="OK">Oklahoma</option>
						                <option value="OR">Oregon</option>
						                <option value="PA">Pennsylvania</option>
						                <option value="RI">Rhode Island</option>
						                <option value="SC">South Carolina</option>
						                <option value="SD">South Dakota</option>
						                <option value="TN">Tennessee</option>
						                <option value="TX">Texas</option>
						                <option value="UT">Utah</option>
						                <option value="VT">Vermont</option>
						                <option value="VA">Virginia</option>
						                <option value="WA">Washington</option>
						                <option value="WV">West Virginia</option>
						                <option value="WI">Wisconsin</option>
						                <option value="WY">Wyoming</option>
						            </select>
						            <i class="arrow double"></i>
						        </label>                   
						    </div><!-- end .col8 section -->	                        
						                                               
						</div>
						<!-- end section -->
						<!--<div class="section">
								<label class="switch switch-flat switch-green">
									<input type="checkbox" name="newsletter" id="newsletter" checked>
									<span class="switch-label" data-on="YES" data-off="NO"></span>
									<span>Subscribe to our newsletter?</span>
								</label>
							</div>-->
						<div class="bottom">
							<center>
								<button type="submit" class="submit next-step">See
									<strong>Your Options</strong>
									<i class="fa fa-check"></i>
								</button>
							</center>
						</div>
						<!-- end section -->
					</fieldset>
					</form>
					<fieldset>
						<div id='response'></div>
					</fieldset>
					<fieldset>
						<div class="loader" id="loader" style="display: none;">
							<img src="images/loading.gif" alt="spinner" />
						</div>
					</fieldset>
				</div>
<!-- 				<div class="form-footer dark-footer"></div>
				<div class="form-footer dark-footer-copyright">
					<p class="copyright">Copyright &copy; 2017</p>
				</div>
 -->				<!-- end .form-footer section -->
				<!-- end .form-body section -->
			</div>
			<!-- end .smart-forms section -->
		</div>
		<!-- end .smart-wrap section -->
		<script>
			$(document).ready(function () {
	/**
	* function to get option selected in the first step
	*/
	function getStep1(){
		var option=0;
		if($('#Refinance').is(':checked')) option = 1;
		else if($('#Purchase').is(':checked')) option = 2;
		else if($('#Reverse').is(':checked')) option = 3;
		else if($('#HomeEquity').is(':checked')) option = 4;
		
		return option;
	}
	
	$('#zipCode').click(function() {
		//FROM ZIP CODE TO PROPERTY VALUE
		$('#smart-form-t-1').click()
		$(this).blur();
	})

	$('[name=LoanType]').change(function(e,b) {
		//FROM LOAN TYPE TO TYPE OF PROPERTY
		$('#smart-form-t-2').click()
		$(this).blur();
	});

	$('[name=PropertyType]').change(function() {
		//FROM PROPERTY TYPE TO PROPERTY USE
		$('#smart-form-t-3').click()
		$(this).blur();
	});

	$('[name=PropertyUse]').change(function() {
		//FROM PROPERTY USE TO CREDIT RANKING
		$('#smart-form-t-4').click()
		$(this).blur();
	});

	$('[name=CreditRating]').change(function() {
		if (getStep1() == 2) {
			//FROM CREDIT RANKING TO PURCHASE TIMEFRAME
			$('#smart-form-t-5').click()
		} else if (getStep1() == 4) {
			//FROM CREDIT RANKING TO PURCHASE YEAR
			$('#smart-form-t-6').click()
		} else {
			//FROM CREDIT RANKING TO BANKRUPCY
			$('#smart-form-t-9').click()
		}
		
		$(this).blur();
	});

	$('[name=PurchaseTimeFrame]').change(function() {
		//FROM PURCHASE TIME FRAME TO BANKRUPCY
		$('#smart-form-t-9').click()
		$(this).blur();
	});	

	//FROM PURCHASE YEAR TO PURCHASE PRICE
	$("#month-picker").monthpicker({
		changeYear: true,
		showmonth: false,
		yearRange: 'c-90:c+0',
		dateFormat: 'yy',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		showButtonPanel: false,
		onSelect: function () {
			$('#smart-form-t-7').click()
		}
	});

	//FROM PURCHASE PRICE TO BANKRUPCY
	$("#PurchasePrice").ionRangeSlider({
		grid: true,
		type: "single",
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 6,
		onFinish: function (data) {
			$('#smart-form-t-9').click();
		},
		values: [
			"$50,000 or less",
			"$50,001 - 75,000",
			"$75,001 - 100,000",
			"$100,001 - 125,000",
			"$125,001 - 150,000",
			"$150,001 - 175,000",
			"$175,001 - 200,000",
			"$200,001 - 250,000",
			"$250,001 - 300,000",
			"$300,001 - 350,000",
			"$350,001 - 400,000",
			"$400,001 - 450,000",
			"$450,001 - 500,000",
			"$500,001 - 750,000",
			"$750,001 - 1,000,000",
			"$1,000,001 - 1,500,000",
			"$1,500,001 - 2,000,000",
			"Over $2,000,000"
		]
	});

	$('[name=AnyBankruptcy]').change(function() {
		//FROM PURCHASE TIME FRAME TO BANKRUPCY
		if ($(this).val() == "Yes") {
			$('#smart-form-t-10').click()
		} else {
			$('#smart-form-t-13').click()
		}
		$(this).blur();
	});	


	$("#BankruptcyTime").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 6,
		values: [
			"Less than 1 year",
			"1 Year",
			"2 Years",
			"3 Years",
			"4 Years",
			"5 Years",
			"More than 5 years"						
		],
		onFinish: function (data) {
			//FROM BANKRUPTCY TIME FRAME TO PROPERTY VALUE
			$('#smart-form-t-13').click();
		},
	});

	$("#PropertyValue").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 6,
		values: [
			"$50,000 or less",
			"$50,001 - 75,000",
			"$75,001 - 100,000",
			"$100,001 - 125,000",
			"$125,001 - 150,000",
			"$150,001 - 175,000",
			"$175,001 - 200,000",
			"$200,001 - 250,000",
			"$250,001 - 300,000",
			"$300,001 - 350,000",
			"$350,001 - 400,000",
			"$400,001 - 450,000",
			"$450,001 - 500,000",
			"$500,001 - 750,000",
			"$750,001 - 1,000,000",
			"$1,000,001 - 1,500,000",
			"$1,500,001 - 2,000,000",
			"Over $2,000,000"
		],
		onFinish: function (data) {
			//FROM PROPERTY VALUE TO (?)
			if (getStep1() == 1) {
				//FIRST MORTAGE BALANCE
				$('#smart-form-t-14').click()
			} else if (getStep1() == 2) {
				//DOWN PAYMENT
				$('#smart-form-t-15').click()
			} else if (getStep1() == 3) {
				//TOTAL MORTAGE BALANCE
				$('#smart-form-t-16').click()
			} else {
				//ANY MORTGAGE
				$('#smart-form-t-12').click();	
			}		
		},
	});

	$("#TotalMortgageBalance").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 6,
		values: [
			"$50,000 or less",
			"$50,001 - 75,000",
			"$75,001 - 100,000",
			"$100,001 - 125,000",
			"$125,001 - 150,000",
			"$150,001 - 175,000",
			"$175,001 - 200,000",
			"$200,001 - 250,000",
			"$250,001 - 300,000",
			"$300,001 - 350,000",
			"$350,001 - 400,000",
			"$400,001 - 450,000",
			"$450,001 - 500,000",
			"$500,001 - 750,000",
			"$750,001 - 1,000,000",
			"$1,000,001 - 1,500,000",
			"$1,500,001 - 2,000,000",
			"Over $2,000,000"
		],
		onFinish: function (data) {
			//FROM TOTAL MORTAGE BALANCE TO INTEREST RATE
			$('#smart-form-t-18').click()
			$(this).blur();			
		}
	});

	$('[name=AnyMortgages]').change(function() {
		//FROM ANY MORTAGE TO 
		if ($(this).val() == "Yes") {
			//FIRST MORGAGE BALANCE
			$('#smart-form-t-14').click()
		} else {
			//DATE OF BIRTH
			$('#smart-form-t-21').click()
		}
		$(this).blur();

	});

	$("#FirstMortgageBalance").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 6,
		values: [
			"$50,000 or less",
			"$50,001 - 75,000",
			"$75,001 - 100,000",
			"$100,001 - 125,000",
			"$125,001 - 150,000",
			"$150,001 - 175,000",
			"$175,001 - 200,000",
			"$200,001 - 250,000",
			"$250,001 - 300,000",
			"$300,001 - 350,000",
			"$350,001 - 400,000",
			"$400,001 - 450,000",
			"$450,001 - 500,000",
			"$500,001 - 750,000",
			"$750,001 - 1,000,000",
			"$1,000,001 - 1,500,000",
			"$1,500,001 - 2,000,000",
			"Over $2,000,000"
		],
		onFinish: function (data) {
			//FROM FIRST MORTGAGE BALANCE TO CASHOUT
			$('#smart-form-t-17').click()
			$(this).blur();
		}
	});

	$("#CashOutAmount").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 1,
		values: [
			"0(No cash)",
			"$1 - 25,000",
			"$25,001 - 50,000",
			"$50,001 - 75,000",
			"$75,001 - 100,000",
			"$100,001 - 125,000",
			"$125,001 - 150,000",
			"$150,001 - 200,000",
			"$200,001 - 250,000",
			"$250,001 - 300,000",
			"$300,001 - 350,000",
			"$350,001 - 400,000",
			"$400,001 - 450,000",
			"$450,001 - 700,000",
			"$700,001 - 950,000",
			"$950,001 - 1,450,000",
			"$1,450,001 - 1,950,000"						
		],
		onFinish: function (data) {
			//FROM CASHOUT TO INTEREST RATE
			$('#smart-form-t-18').click()
			$(this).blur();
		}
	});

	$("#InterestRatePercentage").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		postfix: "%",
		from: 4,
		values: [
			"2.00",
			"2.50",
			"3.00",
			"3.50",
			"4.00",
			"4.50",
			"5.00",
			"5.50",
			"6.00",
			"6.50",
			"7.00",
			"7.50",
			"8.00",
			"8.50",
			"9.00",
			"9.50",
			"10.00",
			"10.50",
			"11.00"
		],
		onFinish: function (data) {
			//FROM CASHOUT TO INTEREST RATE
			$('#smart-form-t-19').click()
			$(this).blur();
		}
	});

	$('[name=CurrentLoanType]').change(function() {
		//FROM CURRENT LOAN TYPE TO DATE OF BIRTH
		$('#smart-form-t-21').click()
		$(this).blur();
	});

	$("#DownPayment").ionRangeSlider({
		grid: true,
		grid_snap: true,
		force_edges: true,
		hide_min_max: true,
		from: 3,
		values: [
			"5%",
			"10%",
			"15%",
			"20%",
			"25%",
			"30%",
			"40%",
			"50%",
			"More than 50%"
		],
		onFinish: function (data) {
			//FROM DOWNPAYMENT TO MILITARY
			$('#smart-form-t-20').click()
			$(this).blur();
		}
	});

	$('[name=MilitaryOrVeteran]').change(function() {
		//FROM MILITARY TO DATE OF BIRTH
		$('#smart-form-t-21').click()
		$(this).blur();
	});
						
				});
		</script>
		<script type="text/javascript" src="js/custom.js"></script>
	</body>

</html>